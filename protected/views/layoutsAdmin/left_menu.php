<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>
        
        <?php
        $user_status = 'all';
        // check user
        $mod_user = User::model()->find('t.email = :emails', array(':emails'=>Yii::app()->User->id ));
        if ($mod_user->group_id == 8 ) {
            $user_status = 'admin';
        }
        ?>
        <?php if ($user_status == 'admin'): ?>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/home')); ?>"><span class="fa fa-home"></span> <?php echo Tt::t('admin', 'Home Page') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Slides') ?></a></li>
            <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'Products') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/product')); ?>">Product Header</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>">View Products</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/create')); ?>">Add Products</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/category/index')); ?>">Category</a></li>
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/filtercat/index')); ?>">Filter</a></li> -->
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/brand/index')); ?>">Brand</a></li> -->
                </ul>
            </li>
            <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/service')); ?>"><span class="fa fa-heart"></span> <?php echo Tt::t('admin', 'Services') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/service')); ?>">Service and Maintenance</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/retrofit')); ?>">Service Retrofit/Optimize</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/part')); ?>">Part Center</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/warranty')); ?>">Warranty</a></li>
                </ul>
            </li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/address/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'Lokasi Toko') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/video/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'Video') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/event/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'Event') ?></a></li>

            <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>"><span class="fa fa-heart"></span> <?php echo Tt::t('admin', 'About') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>">About</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/core')); ?>">Core Values</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/fact')); ?>">Fact Sheet</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/willis')); ?>">Willis Carrier</a></li>
                </ul>
            </li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Slides') ?></a></li>

            <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
            </li>
                
        <?php else: ?>
            
             <li class="dropdown"><a href="#"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'Unique Code Generator') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/setupgen')); ?>">Parameter Setting</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/promo/index')); ?>">Unique Code Generator</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/promo/index2')); ?>">Top Verification Report</a></li>
                </ul>
            </li>

            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/home')); ?>"><span class="fa fa-home"></span> <?php echo Tt::t('admin', 'Home Page') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Slides') ?></a></li>
            <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>"><span class="fa fa-tag"></span> <?php echo Tt::t('admin', 'Products') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/product')); ?>">Product Header</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>">View Products</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/product/create')); ?>">Add Products</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/category/index')); ?>">Category</a></li>
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/filtercat/index')); ?>">Filter</a></li> -->
                    <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/brand/index')); ?>">Brand</a></li> -->
                </ul>
            </li>
            <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/service')); ?>"><span class="fa fa-heart"></span> <?php echo Tt::t('admin', 'Services') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/service')); ?>">Service and Maintenance</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/retrofit')); ?>">Service Retrofit/Optimize</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/part')); ?>">Part Center</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/warranty')); ?>">Warranty</a></li>
                </ul>
            </li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/address/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'Lokasi Toko') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/video/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'Video') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/event/index')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'Event') ?></a></li>

            <li class="dropdown"><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>"><span class="fa fa-heart"></span> <?php echo Tt::t('admin', 'About') ?></a>
                <ul>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>">About</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/core')); ?>">Core Values</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/fact')); ?>">Fact Sheet</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/willis')); ?>">Willis Carrier</a></li>
                </ul>
            </li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>
            <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>"><span class="fa fa-image"></span> <?php echo Tt::t('admin', 'Slides') ?></a></li>

            <li><a href="<?php echo CHtml::normalizeUrl(array('setting/index')); ?>"><span class="fa fa-cogs"></span> <?php echo Tt::t('admin', 'General Setting') ?></a>
            </li>
        <?php endif ?>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
