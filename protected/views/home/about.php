<div class="outer_subpage_wrapper">
<?php  if (isset($_GET['sub']) && $_GET['sub'] == 'core_value'): ?>
  <div class="subpage_top_banner_illustration pg_about"  style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['corebanner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['core_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['core_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
<?php elseif ($_GET['sub'] == 'fact_sheet'): ?>
  <div class="subpage_top_banner_illustration pg_about"  style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['fact_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['fact_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['fact_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
<?php elseif ($_GET['sub'] == 'willis_carrier'): ?>
  <div class="subpage_top_banner_illustration pg_about"  style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['willisbanner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['willis_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['willis_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
<?php else: ?>
  <div class="subpage_top_banner_illustration pg_about"  style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['about_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['about_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['about_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
<?php endif; ?>

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>about</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <li <?php echo (!isset($_GET['sub']))? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About</a></li>
                  <li <?php echo ($_GET['sub'] == 'core_value')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'core_value')); ?>">Core Values</a></li>
                  <li <?php echo ($_GET['sub'] == 'fact_sheet')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'fact_sheet')); ?>">Fact Sheet</a></li>
                  <li <?php echo ($_GET['sub'] == 'willis_carrier')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'willis_carrier')); ?>">Willis Carrier</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
              <?php  if (isset($_GET['sub']) && $_GET['sub'] == 'core_value'): ?>
               <h6><?php echo $this->setting['core_title'] ?></h6>
                <div class="clear height-5"></div>
                <?php echo $this->setting['core_content'] ?>
              <?php elseif ($_GET['sub'] == 'fact_sheet'): ?>
              <h6><?php echo $this->setting['fact_title'] ?></h6>
                <div class="clear height-5"></div>
                <?php echo $this->setting['fact_content'] ?>

                <p><em>U.S. Green Building Council&reg; and LEED&reg; are registered trademarks owned by the U.S. Green Building Council.</em></p>
                <?php elseif ($_GET['sub'] == 'willis_carrier'): ?>
                  <h6><?php echo $this->setting['willis_title'] ?></h6>
                  <div class="clear height-5"></div>
                  <?php echo $this->setting['willis_content'] ?>

              <?php else: ?>
                <h6><?php echo $this->setting['about_title'] ?></h6>
                <div class="clear height-5"></div>
                <div class="tops_c">
                  <p>Built on Willis Carrier&rsquo;s invention of modern air conditioning in 1902, Carrier is the world leader in heating, air-conditioning and refrigeration solutions. We constantly build upon our history of proven innovation with new products and services that improve global comfort and efficiency.</p>
                  <div class="maw600 slide">
                    <ul class="rslides">
                      <li><img src="<?php echo $this->assetBaseurl ?>about/carrier-aquaedge-chiller-519x219-070114.jpg" alt=""></li>
                      <li><img src="<?php echo $this->assetBaseurl ?>about/carrier-infinity-heat-pump-519x219-070114.jpg" alt=""></li>
                      <li><img src="<?php echo $this->assetBaseurl ?>about/carrier-mexico-leed-factory-519x219-070114.jpg" alt=""></li>
                      <li><img src="<?php echo $this->assetBaseurl ?>about/carrier-naturalline-container-refrigeration-519x219-070114.jpg" alt=""></li>
                      <li><img src="<?php echo $this->assetBaseurl ?>about/carrier-supermarket-refrigerated-cabinet-519x219-070114.jpg" alt=""></li>
                    </ul>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear height-20"></div>
                <?php echo $this->setting['about_content'] ?>
                <?php endif; ?>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ResponsiveSlides.js/1.55/responsiveslides.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ResponsiveSlides.js/1.55/responsiveslides.min.js"></script>
<script type="text/javascript">
  $(function(){
    $(".rslides").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
      });
  })
</script>







<?php /*<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_about">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>ABOUT US</h2>
        <h4>WE&rsquo;RE HERE<br />IN INDONESIA</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25">
          <div class="maw1035">
            <h5>Built on Willis Carrier&rsquo;s invention of modern air conditioning in 1902, Carrier is the world leader in heating, air-conditioning and refrigeration solutions. We constantly build upon our history of proven innovation with new products and services that improve global comfort and efficiency.</h5>
          </div>
          <div class="clear height-20"></div>

          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="leftc maw625">
                <h6>THE INVENTION THAT CHANGED THE WORLD</h6>
                <p>In 1902, Willis Carrier solved one of mankind&rsquo;s most elusive challenges by controlling the indoor environment through modern air conditioning. His invention enabled countless industries, promoting global productivity, health and personal comfort.</p>

                <p>Today, Carrier innovations are found across the globe and in virtually every facet of daily life. We create comfortable and productive environments, regardless of the climate. We safeguard the global food supply by preserving the quality and freshness of food and beverages. We ensure health and well-being by enabling the proper transport and delivery of vital medical supplies under exacting conditions. We provide solutions, services and education to lead the green building movement.</p>

                <p>These mark just a handful of the ways that Carrier works to make the world a better place to live, work and play.</p>

                <div class="clear"></div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="pictures_lg prelatife">
                <div id="carousel-insides_aboutRight" class="carousel fade Defaults_slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-insides_aboutRight" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-insides_aboutRight" data-slide-to="1"></li>
                    <li data-target="#carousel-insides_aboutRight" data-slide-to="2"></li>
                  </ol>

                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="<?php echo $this->assetBaseurl; ?>pict-about-1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="item">
                      <img src="<?php echo $this->assetBaseurl; ?>pict-about-1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="item">
                      <img src="<?php echo $this->assetBaseurl; ?>pict-about-1.jpg" alt="" class="img-responsive">
                    </div>
                  </div>

                </div>
              </div>
              <div class="clear"></div>
            </div>
          </div>
          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <section class="carriers_percense about_cn">
      <div class="prelatife container">
        <div class="inside">
          <h3>CARRIER&rsquo;S PRESENCE IN INDONESIA - WITH BERCA</h3>
          <div class="clear height-15"></div>
          <div class="row lists_percense_aboutCn">
            <div class="col-md-3 col-sm-6">
              <div class="leftsn_p">
                <p>Today, Carrier innovations are found across the globe and in virtually every facet of daily life. We create comfortable and productive environments, regardless of the climate. We safeguard the global food supply by preserving the quality and freshness of food and beverages. We ensure health and well-being by enabling the proper transport and delivery of vital medical supplies under exacting conditions. We provide solutions, services and education to lead the green building movement.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl; ?>c_pict-1.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p>In 1902, Willis Carrier solved one of mankind’s most elusive challenges by controlling the indoor environment through modern air conditioning.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl; ?>c_pict-2.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p>In 1902, Willis Carrier solved one of mankind’s most elusive challenges by controlling the indoor environment through modern air conditioning.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl; ?>c_pict-3.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p>In 1902, Willis Carrier solved one of mankind’s most elusive challenges by controlling the indoor environment through modern air conditioning.</p>
                </div>
              </div>
            </div>

          </div>
          <div class="clear"></div>
        </div>
      </div>
    </section>
    
    <section class="bottom_vision_abouts">
      <div class="prelatife container">
        <div class="inside conts_vision">
          <div class="row">
            <div class="col-md-6 bdr_rights">
              <div class="text">
                <h3>OUR VISION</h3>
                <p>In 1902, Willis Carrier solved one of mankind&rsquo;s most elusive challenges by controlling the indoor environment through modern air conditioning. His invention enabled countless industries, promoting global productivity, health and personal comfort</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="text">
                <h3>OUR MISSION</h3>
                <p>In 1902, Willis Carrier solved one of mankind&rsquo;s most elusive challenges by controlling the indoor environment through modern air conditioning. His invention enabled countless industries, promoting global productivity, health and personal comfort</p>
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </section>
    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

*/ ?>