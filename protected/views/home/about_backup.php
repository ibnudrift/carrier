<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_about">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>ABOUT US</h2>
        <h4>WE&rsquo;RE HERE<br />IN INDONESIA</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25">
          <div class="maw1035">
            <h5>Built on Willis Carrier&rsquo;s invention of modern air conditioning in 1902, Carrier is the world leader in heating, air-conditioning and refrigeration solutions. We constantly build upon our history of proven innovation with new products and services that improve global comfort and efficiency.</h5>
          </div>
          <div class="clear height-20"></div>

          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="leftc maw625">
                <h6>THE INVENTION THAT CHANGED THE WORLD</h6>
                <p>In 1902, Willis Carrier solved one of mankind&rsquo;s most elusive challenges by controlling the indoor environment through modern air conditioning. His invention enabled countless industries, promoting global productivity, health and personal comfort.</p>

                <p>Today, Carrier innovations are found across the globe and in virtually every facet of daily life. We create comfortable and productive environments, regardless of the climate. We safeguard the global food supply by preserving the quality and freshness of food and beverages. We ensure health and well-being by enabling the proper transport and delivery of vital medical supplies under exacting conditions. We provide solutions, services and education to lead the green building movement.</p>

                <p>These mark just a handful of the ways that Carrier works to make the world a better place to live, work and play.</p>

                <div class="clear"></div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="pictures_lg prelatife">
                <div id="carousel-insides_aboutRight" class="carousel fade Defaults_slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-insides_aboutRight" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-insides_aboutRight" data-slide-to="1"></li>
                    <li data-target="#carousel-insides_aboutRight" data-slide-to="2"></li>
                  </ol>

                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="<?php echo $this->assetBaseurl; ?>pict-about-1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="item">
                      <img src="<?php echo $this->assetBaseurl; ?>pict-about-1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="item">
                      <img src="<?php echo $this->assetBaseurl; ?>pict-about-1.jpg" alt="" class="img-responsive">
                    </div>
                  </div>

                </div>
              </div>
              <div class="clear"></div>
            </div>
          </div>
          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <section class="carriers_percense about_cn">
      <div class="prelatife container">
        <div class="inside">
          <h3>CARRIER&rsquo;S PRESENCE IN INDONESIA - WITH BERCA</h3>
          <div class="clear height-15"></div>
          <div class="row lists_percense_aboutCn">
            <div class="col-md-3 col-sm-6">
              <div class="leftsn_p">
                <p>Today, Carrier innovations are found across the globe and in virtually every facet of daily life. We create comfortable and productive environments, regardless of the climate. We safeguard the global food supply by preserving the quality and freshness of food and beverages. We ensure health and well-being by enabling the proper transport and delivery of vital medical supplies under exacting conditions. We provide solutions, services and education to lead the green building movement.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl; ?>c_pict-1.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p>In 1902, Willis Carrier solved one of mankind’s most elusive challenges by controlling the indoor environment through modern air conditioning.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl; ?>c_pict-2.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p>In 1902, Willis Carrier solved one of mankind’s most elusive challenges by controlling the indoor environment through modern air conditioning.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6">
              <div class="items">
                <div class="pict"><img src="<?php echo $this->assetBaseurl; ?>c_pict-3.jpg" alt="" class="img-responsive"></div>
                <div class="info">
                  <p>In 1902, Willis Carrier solved one of mankind’s most elusive challenges by controlling the indoor environment through modern air conditioning.</p>
                </div>
              </div>
            </div>

          </div>
          <div class="clear"></div>
        </div>
      </div>
    </section>
    
    <section class="bottom_vision_abouts">
      <div class="prelatife container">
        <div class="inside conts_vision">
          <div class="row">
            <div class="col-md-6 bdr_rights">
              <div class="text">
                <h3>OUR VISION</h3>
                <p>In 1902, Willis Carrier solved one of mankind&rsquo;s most elusive challenges by controlling the indoor environment through modern air conditioning. His invention enabled countless industries, promoting global productivity, health and personal comfort</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="text">
                <h3>OUR MISSION</h3>
                <p>In 1902, Willis Carrier solved one of mankind&rsquo;s most elusive challenges by controlling the indoor environment through modern air conditioning. His invention enabled countless industries, promoting global productivity, health and personal comfort</p>
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </section>
    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>