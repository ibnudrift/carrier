<?php 
$data_service = array(
      1 => array(
            'title'=>'Start-up services & extended warranty protection',
            'short_desc'=>'<p>Carrier commercial equipment represents today&rsquo;s most advanced technology. To ensure that you receive the full benefits from this leading edge design, proper start-up, following a rigorous, factory-defined set of procedures is essential. Correct start-up is the key to optimum performance, safety and reliability, not just during the first days or weeks of operation, but for years to come.</p>',
            'desc'=>'',
            ),
          array(
            'title'=>'Prevent & predict',
            'short_desc'=>'<p>Ensuring your equipment&rsquo;s peak performance and longevity dictates a commitment to preventive, predictive and proactive maintenance programs. Safeguarding your equipment by choosing Carrier brings the security of having forged a true partnership with the HVAC industry&rsquo;s foremost servicing and technology leader. At Carrier, we partner with building owners and managers to keep your system running at its best, with customized service plans designed to meet all your specific equipment and operational needs year-round. We service all brands and types of HVAC units.</p>',
            'desc'=>'',
            ),
          array(
            'title'=>'Repairs & emergency service',
            'short_desc'=>'<p>Even the best maintained equipment can encounter an unexpected failure. As Carrier&rsquo;s own servicing entity, we have access to the latest engineering advancements and the most advanced technical servicing tools. Our expansive OEM service network has strategically-located offices in the United States and Canada. Translation: we&rsquo;ll be there whenever you need us&hellip; 24/7/365. Centralized service dispatch and technician tracking ensures immediate response to unplanned service events. Our service expertise extends well beyond our in-depth knowledge of Carrier equipment. We have decades of experience servicing all major HVAC brands of equipment. Simply put &ndash; we bring an unmatched level of technical confidence to each and every repair situation.</p>',
            'desc'=>'',
            ),
          array(
            'title'=>'Carrier&reg; SMART service',
            'short_desc'=>'<p>Carrier&rsquo;s SMART Service is a dynamic, proactive strategy for enhanced equipment and system management. Through the identification and analysis of chiller and system operating trends, more informed decisions can now be made relative to meeting comfort demands, implementing service, maintenance or repair events and improving a building&rsquo;s financial performance. This unique service can be included in a Carrier Service Agreement plan as part of a regular preventive and predictive maintenance program. This technology is also portable and can be used on a temporary basis to troubleshoot, diagnose issues and provide analysis for service recommendations.</p>',
            'desc'=>'',
            ),
          array(
            'title'=>'Equipment overhaul',
            'short_desc'=>'<p>We help provide you with a peace of mind by providing timely maintenance and avoid unnecessary downtown on HVAC equipment. By scheduling comprehensive inspections and overhauls during planned down times, we help to keep your equipment running in optimum condition and maximize their useful lives. Our service experts begin with a total performance evaluation taking consideration of the age of the equipment, actual operational hours, and conditions of operation. A customized overhaul schedule is designed for HVAC equipment and operation.</p>',
            'desc'=>'',
            ),
          array(
            'title'=>'24/7 Support & service',
            'short_desc'=>'<p>Keeping your equipment running smoothly and efficiently has no predictable timetable. Our Service Center is your &lsquo;first-response&rsquo; to problem resolution. Our customer service agents are available 24-hours-a-day, 7-days-a-week, 365-days-a-year to coordinate your request for service so that our local areas can efficiently dispatch our technicians to help solve any unplanned event.</p>',
            'desc'=>'',
            ),

  );
?>
<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_service">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>SERVICES</h2>
        <h4>exceeding your<br>expectations</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>services</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <li class="active"><a href="#">Service and Maintenance</a></li>
                  <li><a href="#">Service Retrofit/Optimize</a></li>
                  <li><a href="#">Parts Center</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <h6>OPERATE, SERVICE & PROTECT</h6>
                <div class="clear height-5"></div>
                <div class="tops_c">
                  <p><b>MAXIMIZE YOUR EQUIPMENT INVESTMENT WITH CARRIER SERVICE.</b><br>
                  As servicing experts, we know how to get the most out of your investment. The capital equipment you purchased will, with proper maintenance, be in operation for a significant length of time.</p>
                  <p><img src="<?php echo $this->assetBaseurl; ?>picts_services-1.jpg" alt="" class="img-responsive"></p>
                </div>

                <!-- Start service list data -->
                <div class="lists_data_service">
                  <?php foreach ($data_service as $key => $value): ?>
                  <div class="items">
                    <h6><?php echo $value['title'] ?></h6>
                    <?php echo $value['short_desc'] ?>
                    
                    <?php if ($key == 6): ?>
                      <div class="descriptions_bottom fading">
                        <div class="row default">
                          <div class="col-md-6 col-sm-6">
                            <h5>CARRIER COMMERCIAL SERVICE = CORE OF CARRIER QUALITY.</h5>
                            <p>Partnering with Carrier Commercial Service brings with it the security of knowing that every facet of your investment is being carefully monitored, analyzed, maintained and documented. This practice initiates with your very first contact with us and will endure over the entire lifetime of your equipment and on to the next. Our expansive knowledge is the basis for our portfolio of services. Individually and collectively, this is the best platform from which to operate equipment efficiently, safely and predictably.</p>
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <div class="pict_featured"><img src="<?php echo $this->assetBaseurl ?>pic_services_support_service.jpg" alt="" class="img-responsive"></div>
                          </div>
                        </div>
                        <div class="clear height-25"></div>
                        <p>Keeping your equipment running smoothly and efficiently has no predictable timetable. Our service center is your ‘first-response’ to problem resolution, 24/7/365. Our customer service agents are available 24-hours-a-day, 7-days-a-week, 365-days-a-year to coordinate your request for service so that our local areas can efficiently dispatch our technicians to help solve any unplanned event.</p>
                        <div class="row default">
                          <div class="col-md-6 col-sm-6">
                            <h4>How can Carrier’s 24/7/365 support provide value to you?</h4>
                            <p>It provides:</p>
                            <ul>
                              <li>Quick response means a faster resolution of your service needs (emergency or routine)</li>
                              <li>You will receive immediate attention, evaluation and the best action path to address your servicing needs from the experts at Carrier.</li>
                              <li>You can always reach the Service Center with 24/7/365 access to, and response from, the service representatives. </li>
                              <li>This round-the-clock access translates into uninterrupted service at your facility.</li>
                            </ul>
                          </div>
                          <div class="col-md-6 col-sm-6">
                            <h4>Carrier’s 24/7/365 Support applies to:</h4>
                            <ul>
                              <li>All Carrier service customers receive 24/7/365 support</li>
                              <li>Contact us: 1300 130 750 </li>
                            </ul>
                          </div>
                        </div>
                        <div class="clear"></div>
                      </div>
                    <?php else: ?>
                      <div class="descriptions_bottom fading">
                        <p>Keeping your equipment running smoothly and efficiently has no predictable timetable. Our service center is your ‘first-response’ to problem resolution, 24/7/365. Our customer service agents are available 24-hours-a-day, 7-days-a-week, 365-days-a-year to coordinate your request for service so that our local areas can efficiently dispatch our technicians to help solve any unplanned event.</p>
                        <p>Keeping your equipment running smoothly and efficiently has no predictable timetable. Our service center is your ‘first-response’ to problem resolution, 24/7/365. Our customer service agents are available 24-hours-a-day, 7-days-a-week, 365-days-a-year to coordinate your request for service so that our local areas can efficiently dispatch our technicians to help solve any unplanned event.</p>
                        <p>Keeping your equipment running smoothly and efficiently has no predictable timetable. Our service center is your ‘first-response’ to problem resolution, 24/7/365. Our customer service agents are available 24-hours-a-day, 7-days-a-week, 365-days-a-year to coordinate your request for service so that our local areas can efficiently dispatch our technicians to help solve any unplanned event.</p>
                        <div class="clear"></div>
                      </div>
                    <?php endif ?>
                    <div class="bottoms_line">
                      <div class="lines"></div>
                      <a href="javascript:return false;" class="btn btn-link ctm_btns views_desc" data-id="service_desc_<?php echo $key; ?>">VIEW MORE</a>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <?php endforeach ?>

                  <div class="clear"></div>
                </div>
                <!-- End service list data -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

<script type="text/javascript">
  $(function(){

    $('a.ctm_btns.views_desc').on('click', function(){
      if ($( this ).hasClass( "aktif" )) {
        // hide desc
        $(this).parent().parent().find('.descriptions_bottom.fading').slideUp('slow');
        // remove active and change text
        $(this).removeClass('aktif').text('READ MORE');
      } else {
        // show desc
        $(this).parent().parent().find('.descriptions_bottom.fading').slideDown('slow');
        // add active and change text
        $(this).addClass('aktif').text('VIEW LESS');
      }
      return false;
    });

  })
</script>