<!-- start form c -->
<div class="box-form tl-contact-form loginPage">
  <div class="clear height-0"></div>
  <div class="text-left">
    <div class="text-left">
    <div class="clear"></div>
  <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                  'type'=>'',
                  'enableAjaxValidation'=>false,
                  'clientOptions'=>array(
                      'validateOnSubmit'=>false,
                  ),
                  'htmlOptions' => array(
                      'enctype' => 'multipart/form-data',
                      'class' => 'form-inline',
                      'onsubmit'=> 'javascript:alert("Sorry, wrong part number"); return false;',
                  ),
              )); ?>
   <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger')); ?>
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <?php $this->widget('bootstrap.widgets.TbAlert', array(
            'alerts'=>array('success'),
        )); ?>
    <?php endif; ?>

    <div class="row default">
      <div class="col-md-12">
        <div class="form-group">
            <label for="exampleInputName">ITEM CODE</label>
            <?php echo $form->textField($model, 'item_code', array('class'=>'form-control')); ?>
        </div>
      </div>
    </div>
    <div class="row default">
      <div class="col-md-12">
        <div class="form-group">
            <label for="exampleInputName">PART NUMBER</label>
            <?php echo $form->textField($model, 'part_number', array('class'=>'form-control')); ?>
        </div>
      </div>
    </div>

    <div class="row default">

      <div class="col-md-12 col-sm-12 col-lg-12">
        <div class="row default">
          <div class="col-md-6 col-sm-6">
            <div class="fright-inpd">
              <div class="form-group mb-0">
                <div class="fleft">
                  <!-- <div class="g-recaptcha" data-sitekey="6Lc5ExQUAAAAALB4V8LnnmdlQT8TYGIqNqXWZ_Rf"></div> -->
                </div>
                <div class="clear"></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="fright">
              <button type="submit" class="btn btn-default btns-submit-bt"></button>
            </div>
          </div>
        </div>

      </div>
    </div>

  <?php $this->endWidget(); ?>
      <div class="clear height-30"></div>
    <div class="clear"></div>
  </div>

  <div class="clear"></div>
</div>

<div class="clear"></div>
</div>
<!-- end form c -->
<script src='https://www.google.com/recaptcha/api.js'></script>