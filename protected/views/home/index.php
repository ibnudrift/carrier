<section class="default_sc" id="block_homesection1">
	<div class="prelatife container">
		<div class="insides">
			<div class="lists_banner_home_d1 default_banner-home">
				<div class="row">
				<?php for ($i=1; $i < 4; $i++) { ?>
					<div class="col-md-4">
						<div class="items">
						<a href="<?php echo $this->setting['home_banner_url_'.$i] ?>">
							<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(461,293, '/images/static/'.$this->setting['home_banner_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
							</a>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>