<div class="outer_subpage_wrapper">
  <?php  if (isset($_GET['sub']) && $_GET['sub'] == 'retrofit') { ?>
  <div class="subpage_top_banner_illustration pg_service" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['retrofit_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['retrofit_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['retrofit_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
  <?php }elseif ($_GET['sub'] == 'parts-center') { ?>
  <div class="subpage_top_banner_illustration pg_service" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['part_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['part_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['part_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
  <?php }elseif ($_GET['sub'] == 'warranty') { ?>
  <div class="subpage_top_banner_illustration pg_service" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['warranty_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['warranty_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['warranty_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
  <?php  }else{ ?>
  <div class="subpage_top_banner_illustration pg_service" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['service_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['service_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['service_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->
  <?php } ?>

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>services</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <li <?php echo (!isset($_GET['sub']))? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services')); ?>">Service and Maintenance</a></li>
                  <li <?php echo ($_GET['sub'] == 'retrofit')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'retrofit')); ?>">Service Retrofit/Optimize</a></li>
                  <li <?php echo ($_GET['sub'] == 'parts-center')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'parts-center')); ?>">Parts Center</a></li>
                  <li <?php echo ($_GET['sub'] == 'warranty')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'warranty')); ?>">Warranty</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <?php  if (isset($_GET['sub']) && $_GET['sub'] == 'retrofit') { ?>
                <h6><?php echo $this->setting['retrofit_title'] ?></h6>   
                <div class="clear height-5"></div>
                <?php echo $this->setting['retrofit_content'] ?>
                <div class="lists_retrofit_services">
                  <?php for ($i=1; $i < 7 ; $i++) { ?>
                  <?php if ($this->setting['retrofit_list_title_'.$i] != ''): ?>
                  <div class="list">
                    <div class="tops"><?php echo $i.'. '.$this->setting['retrofit_list_title_'.$i] ?></div>
                    <div class="clear height-10"></div>
                    <div class="box_green">
                      <h5><?php echo $this->setting['retrofit_list_subtitle_'.$i] ?></h5>
                      <p><?php echo $this->setting['retrofit_list_subcontent_'.$i] ?></p>
                    </div>
                    <div class="clear height-10"></div>
                    <?php echo $this->setting['retrofit_list_content_'.$i] ?>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                  <?php endif ?>
                  <?php } ?>

                </div>
                <?php }elseif ($_GET['sub'] == 'parts-center') { ?>
                <h6><?php echo $this->setting['part_title'] ?></h6>
                <div class="clear height-5"></div>
                <?php echo $this->setting['part_content'] ?>
                <?php }elseif ($_GET['sub'] == 'warranty') { ?>
                <h6><?php echo $this->setting['warranty_title'] ?></h6>
                <div class="clear height-5"></div>
                <?php echo $this->setting['warranty_content'] ?>
                <?php  }else{ ?>
                <h6><?php echo $this->setting['service_title'] ?></h6>
                <div class="clear height-5"></div>
                <?php echo $this->setting['service_content'] ?>
                <?php } ?>
                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

<script type="text/javascript">
  $(function(){

    $('a.ctm_btns.views_desc').on('click', function(){
      if ($( this ).hasClass( "aktif" )) {
        // hide desc
        $(this).parent().parent().find('.descriptions_bottom.fading').slideUp('slow');
        // remove active and change text
        $(this).removeClass('aktif').text('READ MORE');
      } else {
        // show desc
        $(this).parent().parent().find('.descriptions_bottom.fading').slideDown('slow');
        // add active and change text
        $(this).addClass('aktif').text('VIEW LESS');
      }
      return false;
    });

  })
</script>