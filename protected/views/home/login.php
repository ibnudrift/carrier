<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_loginPage">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <?php 
        switch ($_GET['sub']) {
          case 'dealer':
            echo '<h2 class="t_upper">DEALER LOGIN</h2>';
            break;
          case 'installer':
            echo '<h2 class="t_upper">INSTALLER LOGIN</h2>';
            break;
          
          default:
            echo '<h2 class="t_upper">warranty registration</h2>';
            break;
        }
        ?>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pContact">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <p>&nbsp;</p>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <div class="clear height-50"></div><div class="height-5"></div>
                <?php if ($_GET['sub'] == 'dealer'): ?>
                  <p>Please login to your dealer administration area</p>
                  <div class="clear height-20"></div>
                  <?php echo $this->renderPartial('//home/_form_loginPg', array('model'=>$model)); ?>
                <?php elseif ($_GET['sub'] == 'installer'): ?>
                  <p>Please login to your installer administration area</p>
                  <div class="clear height-20"></div>
                  <?php echo $this->renderPartial('//home/_form_loginPg', array('model'=>$model)); ?>
                <?php else: ?>
                <p>Please input your item details below</p>
                <div class="clear height-20"></div>
                <?php echo $this->renderPartial('//home/_form_loginPWarranty', array('model'=>$model)); ?>
                <?php endif ?>

                <div class="clear height-50"></div>
                <div class="clear height-50"></div>
                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>
