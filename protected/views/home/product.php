<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>products</h2>
        <h4>THE BEST AT ITS&rsquo;<br>class</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
                  <div class="list">
                    <a class="top" href="#">Commercial</a>
                    <ul class="list-unstyled">
                      <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Air Handler</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Chillers</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Fan Coil</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Package Unit</a></li>
                    </ul>
                    <div class="clear"></div>
                  </div>
                  <div class="list">
                    <a class="top" href="#">Low Commercial</a>
                    <ul class="list-unstyled">
                      <li><a href="#">Light Commercial</a></li>
                      <li><a href="#">Residential</a></li>
                      <li><a href="#">VRF</a></li>
                    </ul>
                    <div class="clear"></div>
                  </div>

                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <!-- <h6>AIR HANDLERS</h6> -->
                <!-- COMMERCIAL  >  AIR HANDLERS -->
                <h6>COMMERCIAL &nbsp;>&nbsp;&nbsp;AIR HANDLERS</h6>
                <div class="clear height-5"></div>

                <div class="tops_landing_products">
                  <div class="info">
                    <h5>NO MATTER THE APPLICATION, CARRIER CAN HANDLE IT.</h5>
                    <p><b>Carrier offers a range of air-handling units to meet the demands of precise indoor environments.</b></p>
                    <p>Whether you need a custom-built or preconfigured product, Carrier air handlers are available with a wide airflow range, flexible designs and advanced filtration options to bring clean air and comfort to a variety of spaces.</p>
                    <div class="clear"></div>
                  </div>
                  <div class="block_pict"></div>
                </div>

                <div class="middles_products">
                  <div class="clear height-35"></div>
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                      <div class="lefts_c">
                        <h6>REFINE BY</h6>
                        <div class="filtering_data">
                          <div class="list_filter">
                            <p><b>TYPE</b></p>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Custom-built (3)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Packaged (1)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Suspended packaged (1)
                              </label>
                            </div>

                            <div class="clear"></div>
                          </div>

                          <div class="list_filter">
                            <p><b>Construction</b></p>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Double-wall (3)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Single-wall (2)
                              </label>
                            </div>

                            <div class="clear"></div>
                          </div>

                          <div class="list_filter">
                            <p><b>Max Airflow (m&sup3;/h)</b></p>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                5,040 (1)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                7,500 (1)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                60,000 (1)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                100,000 (1)
                              </label>
                            </div>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                126,000 (1)
                              </label>
                            </div>

                            <div class="clear"></div>
                          </div>

                          <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                      </div>
                      <!-- End Left c -->
                    </div>
                    <div class="col-md-9 col-sm-9">
                      <div class="rights_c">
                        <h6>5 PRODUCTS</h6>

                        <div class="lists_sub_products">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>Model</th>
                                  <th>Type</th>
                                  <th>Construction</th>
                                  <th>Airflow range (m3/h)</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php for ($i=0; $i < 5; $i++) { ?>
                                <tr>
                                  <td>
                                  <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail')); ?>">
                                    <span class="nmes">39CQ COMPACT</span>
                                    <div class="clear"></div>
                                    <img src="<?php echo $this->assetBaseurl ?>ex_prd_child1.jpg" alt="" class="img-responsive">
                                    </a>
                                  </td>
                                  <td>Packaged</td>
                                  <td>Single-wall</td>
                                  <td>500~6500</td>
                                </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                            <div class="clear"></div>
                        </div>
                        <!-- End list sub products -->

                        <div class="clear"></div>
                      </div>
                      <!-- End Right c -->
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>
