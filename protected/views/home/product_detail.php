<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>products</h2>
        <h4>THE BEST AT ITS&rsquo;<br>class</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
                  <div class="list">
                    <a class="top" href="#">Commercial</a>
                    <ul class="list-unstyled">
                      <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Air Handler</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Chillers</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Fan Coil</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Package Unit</a></li>
                    </ul>
                    <div class="clear"></div>
                  </div>
                  <div class="list">
                    <a class="top" href="#">Low Commercial</a>
                    <ul class="list-unstyled">
                      <li><a href="#">Light Commercial</a></li>
                      <li><a href="#">Residential</a></li>
                      <li><a href="#">VRF</a></li>
                    </ul>
                    <div class="clear"></div>
                  </div>

                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont block_detail_products">
                <a href="#" class="btn btn-link btnsr_back_product"><i class="fa fa-arrow-left"></i> &nbsp;BACK TO AIR HANDLERS PRODUCTS</a>
                <div class="clear height-20"></div>

                <div class="row">
                  <div class="col-md-6 col-sm-6">
                    <div class="pictures_box">
                      <div class="pict_big"><img src="<?php echo $this->assetBaseurl ?>ex_big_product.jpg" alt="" class="img-responsive"></div>
                      <div class="list_chiild">
                        <ul class="list-inline">
                          <li><img src="<?php echo $this->assetBaseurl ?>ex_bottom_childPictures.jpg" alt="" class="img-responsive"></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="description">
                      <h4>39CQ COMPACT</h4>
                      <div class="clear height-10"></div>
                      <h6>COMPACT AIR HANDLER<br />Airflow range: 500 to 6,500 m&sup3;/h</h6>
                      <div class="clear height-10"></div>
                      <ul>
                          <li>Well suited to the air conditioning of separate zones</li>
                          <li>Features high static pressure and requires low ceiling heights</li>
                          <li>Wide array of housings and accessories</li>
                          <li>Available in horizontal or vertical configurations for indoor installation</li>
                          <li>This range is particularly well-suited to tertiary buildings:
                          <ul>
                            <li>Administration, offices</li>
                            <li>Education facilities, libraries, community centers</li>
                            <li>Bars, hotels, restaurants</li>
                            <li>Shopping centers</li>
                            <li>Nursing homes, healthcare</li>
                          </ul>
                          </li>
                        </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>

                <div class="description pl-0">
                <div class="bottoms_desc">
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">FEATURES</a></li>
                            <li role="presentation"><a href="#option" aria-controls="option" role="tab" data-toggle="tab">OPTIONS</a></li>
                            <li role="presentation"><a href="#spesific" aria-controls="spesific" role="tab" data-toggle="tab">SPECIFICATIONS</a></li>
                            <li role="presentation"><a href="#brochures" aria-controls="brochures" role="tab" data-toggle="tab">BROCHURES</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="feature">
                              <ul>
                                <li>The range is composed of 3 sizes from 500 to 6500 m3/h (thickness: 400 mm).</li>
                                <li>There are three different installation possibilities to meet your needs:
                                <ul>
                                  <li>Horizontal ceiling-mounted version, accessed from underneath</li>
                                  <li>Horizontal floor-mounted version, accessed from the top</li>
                                  <li>ertical wall-mounted version, accessed via the front</li>
                                </ul>
                                </li>
                                <li>Available pressure from 100 to 1000 Pa with high performance and minimum sound level</li>
                                <li>Unit is available in several versions: single-flow, aligned dual flow, adjacent dual-flow</li>
                                <li>Extra flat unit</li>
                                <li>Built-in control in option</li>
                                <li>Removable hinged doors for enhanced accessibility</li>
                                <li>Universal size filters cells: easier for maintenance</li>
                                <li>Direct driven fan in standard (less maintenance)</li>
                                <li>EC motor in option (variable speed, high efficiency)</li>
                              </ul>
                              <img src="https://placehold.it/1200x300" alt="">
                              <div class="clear"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="option">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis quisquam, recusandae assumenda mollitia alias. Minus perferendis possimus, mollitia vitae accusantium, aperiam veritatis dolores earum consectetur, voluptatem maxime ex suscipit minima.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="spesific">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis quisquam, recusandae assumenda mollitia alias. Minus perferendis possimus, mollitia vitae accusantium, aperiam veritatis dolores earum consectetur, voluptatem maxime ex suscipit minima.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="brochures">
                              <div class="down_brochure">
                                <div class="lists"><i class="fa-brochure"></i> 39CQ PRODUCT INFORMATION BROCHURE</div>
                                <div class="lists"><i class="fa-brochure"></i> 39CQ PRODUCT INFORMATION BROCHURE</div>
                              </div>
                            </div>
                          </div>

                        </div>

                  <div class="clear"></div>
                </div>
                <!-- End bottm product Detail -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>
