<?php 
$data_event = array(
      1 => array(
            'title'=>'Start-up services & extended warranty protection',
            'short_desc'=>'<p>Carrier commercial equipment represents today&rsquo;s most advanced technology. To ensure that you receive the full benefits from this leading edge design, proper start-up, following a rigorous, factory-defined set of procedures is essential. Correct start-up is the key to optimum performance, safety and reliability, not just during the first days or weeks of operation, but for years to come.</p>',
            'desc'=>'',
            ),

  );
?>
<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_events">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>EVENTS</h2>
        <h4>things you don&rsquo;t<br />want to miss</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">
<?php
$folder = Event::model()->getMenu($this->languageID, 'upcoming');
$folderArchived = Event::model()->getMenu($this->languageID, 'archived');
?>

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices conts_pEvent">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>upcoming events</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <?php foreach ($folder as $key => $value): ?>
                  <li <?php if ($_GET['year'] == $value['year'] AND $_GET['month'] == $value['month']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/event', 'year'=>$value['year'], 'month'=>$value['month'])); ?>"><?php echo $value['label'] ?></a></li>
                  <?php endforeach ?>
                </ul>
                
                <div class="clear height-50"></div>
                <h5>archived events</h5>
                <div class="clear height-15"></div>
                <ul class="list-unstyled">
                  <?php foreach ($folderArchived as $key => $value): ?>
                  <li <?php if ($_GET['year'] == $value['year'] AND $_GET['month'] == $value['month']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/event', 'year'=>$value['year'], 'month'=>$value['month'])); ?>"><?php echo $value['label'] ?></a></li>
                  <?php endforeach ?>
                </ul>

              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <h6><?php echo $dataEvent->getTotalItemCount() ?> events in <?php echo(Yii::app()->locale->getMonthNames()[$_GET['month']]) ?> <?php echo $_GET['year'] ?></h6>
                <div class="clear height-0"></div>

                <!-- Start default list data -->
                <div class="lists_data_defaults_lh">
                  <div class="row default">
                  <?php foreach ($dataEvent->getData() as $key => $value): ?>
                    
                  <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="items">
                      <div class="picture prelatife">
                        <ul id="light-gallery" class="gallery list-un light-gallery">
                        <?php foreach ($value->images as $k => $v): ?>
                          <?php if ($k == 0): ?>
                          <li data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,500, '/images/event/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>">
                          <a href="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,500, '/images/event/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>">
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(324,213, '/images/event/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                            </a>
                          </li>
                          <?php else: ?>
                          <li data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(800,500, '/images/event/'.$v->image , array('method' => 'resize', 'quality' => '90')) ?>"></li>
                          <?php endif ?>
                        <?php endforeach ?>
                        </ul>
                      </div>
                      <div class="info">
                        <h6><?php echo $value->description->title ?></h6>
                        <p><?php echo date('d F Y', strtotime($value->date)) ?><br>
                        <?php echo $value->description->content ?></p>
                      </div>
                    </div>
                  </div>
                  <?php if (($key + 1) % 3 == 0): ?>
                  <div class="hidden-sm hidden-md clear"></div>
                  <?php endif ?>
                  <?php if (($key + 1) % 2 == 0): ?>
                  <div class="visible-md visible-sm clear"></div>
                  <?php endif ?>
                  <?php endforeach ?>
                  </div>
                  <div class="clear"></div>
                </div>
                <!-- End default list data -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

    <div class="text-center bgs_paginations">
          <?php $this->widget('CLinkPager', array(
              'pages' => $dataEvent->getPagination(),
              'header' => '',
          )) ?>
    </div>

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

<link rel="stylesheet"  href="<?php echo Yii::app()->baseUrl; ?>/asset/js/light-gallery/css/lightGallery.css"/>
<script src="<?php echo Yii::app()->baseUrl; ?>/asset/js/light-gallery/js/lightGallery.js"></script>
<script>
  $(document).ready(function() {
    $(".light-gallery").lightGallery({
      thumbnail: false,
    });
  });
</script>
<style type="text/css" media="screen">
  ul.gallery{ list-style: none; }
</style>