<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_findExpert">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>find an expert</h2>
        <!-- <h4>get your nearest<br>carrier experts</h4> -->
        <?php if ($_GET['loc'] != ''): ?>
          <?php if ($_GET['loc'] == 'dealer-location'): ?>
            <h4>DEALER LOCATION</h4>
          <?php else: ?>
            <h4>ASP LOCATION</h4>
          <?php endif ?>
        <?php endif ?>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text conts_pFindExpert">
          <div class="maw1000 tengah text-center">
            <p>The Carrier Brand and its&rsquo; product distribution of Industrial Refrigerations and Commercial Air Conditionings in Indonesia has started its&rsquo; presence since 1980 and continue to spread nationwide to major important cities and various strategic locations. Please use the locator below to locate the Carrier dealer nearest to your location.</p>
            <div class="clear height-30"></div>
            <div class="pcts_center"><img src="<?php echo $this->assetBaseurl ?>picts_maps_indo.jpg" alt="" class="img-responsive center-block"></div>
            <div class="clear height-50"></div><div class="height-10"></div>
            <div class="blocksn_form_filter">
              <form action="<?php echo CHtml::normalizeUrl(array('find_expert', 'loc'=>$_GET['loc'])); ?>" id="search-map" class="form-inline">
                <div class="form-group">
                  <label>Select Province</label>
                  <div class="clear"></div>
                  <select name="prov" id="select-prov" class="form-control">
                    <option value="">Pilih Province</option>
                    <?php foreach ($listProv as $key => $value): ?>
                      <option value="<?php echo $value->prov ?>"><?php echo $value->prov ?></option>
                    <?php endforeach ?>
                  </select>
                  <script type="text/javascript">
                    $('#select-prov').val('<?php echo $_GET['prov'] ?>');
                  </script>
                </div>
                <div class="form-group">
                  <label>Select City</label>
                  <div class="clear"></div>
                  <select name="kota" id="select-kota" class="form-control">
                    <option value="">Pilih Kota</option>
                    <?php foreach ($listKota as $key => $value): ?>
                      <option value="<?php echo $value->kota ?>"><?php echo $value->kota ?></option>
                    <?php endforeach ?>
                  </select>
                  <script type="text/javascript">
                    $('#select-kota').val('<?php echo $_GET['kota'] ?>');
                  </script>
                </div>
                <div class="form-group">
                  <label>&nbsp;</label>
                  <div class="clear"></div>
                  <button type="button" class="btn btn-link btns_csubmit sets_submitForm1"></button>
                </div>
              </form>
              <div class="clear height-50"></div><div class="clear height-10"></div>
              
              <?php if ($_GET['prov'] != '' OR $_GET['kota'] != ''): ?>
              <div class="boxs_view_maps">
                <div class="tops_title text-center padding-bottom-25">
                  DISPLAYING LOCATION MAP FOR: <br>
                  <b>Carrier jiexpo</b>
                </div>
                <div class="maps_area">
                  <div id="map" style="width: 100%; height: 350px;"></div>
                </div>

                <div class="clear"></div>
              </div>

              <div class="boxs_list_location_finds_n">
                <p class="help-block">Click “View Location” on your choosen location to display the map.</p>
                <div class="blocks_lists_locat_data">
                <?php if (count($dataAddress) > 0): ?>
                  <div class="row default">
                    <?php foreach ($dataAddress as $key => $value): ?>
                    <div class="col-md-4 col-sm-6">
                      <div class="items">
                        <address>
                          <b><?php echo $value->nama ?></b> <br>
                            <?php echo $value->address_1 ?><br />
                            <?php if ($value->address_2 != ''): ?>
                              <?php echo nl2br($value->address_2) ?><br />
                            <?php endif ?>
                          <?php if ($value->telp != ''): ?>
                          P. <?php echo $value->telp ?><br />
                          <?php endif ?>
                          <?php if ($value->fax != ''): ?>
                          F. <?php echo $value->fax ?> <br>
                          <?php endif ?>
                          <?php if ($value->email != ''): ?>
                          E. <?php echo $value->email ?>
                          <?php endif ?>
                          <a href="#" onclick="myClick(<?php echo $key ?>);return false;" class="views_map_loc">View Location</a>
                        </address>
                      </div>
                    </div>
                    <?php endforeach ?>
                  </div>
                <?php endif ?>
                </div>
                <div class="clear"></div>
              </div>

              <p class="help-block">If you found difficulties finding the right experts near you or need further assistance , please call our customer service hotline at (021) 2889955</p>
              <div class="clear"></div>
              <?php endif ?>
            </div>

            <div class="clear"></div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

<script type="text/javascript">
  $(function(){

    $('#select-prov').on('change', function(){
      $('#select-kota').val('');
      $('#search-map').submit();
    });
    $('#select-kota').on('change', function(){
      $('#search-map').submit();
    });

    $('a.views_map_loc').on('click', function(){
      $('.boxs_view_maps').removeClass('hide').slideDown('slow');
    });

  });
var markers = [];
function initMap() {
  var locations = [
  <?php if (count($dataAddress) > 0): ?>
  <?php foreach ($dataAddress as $key => $value): ?>
    ['<?php echo $value->nama ?>', <?php echo $value->lat ?>, <?php echo $value->lng ?>, <?php echo $key ?>],
  <?php endforeach ?>
  <?php endif ?>
  ];
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(41.923, 12.513), 
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker, i;

  //create empty LatLngBounds object
  var bounds = new google.maps.LatLngBounds();
  var infowindow = new google.maps.InfoWindow();    

  for (i = 0; i < locations.length; i++) {  
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map,
      icon: '<?php echo Yii::app()->baseUrl ?>/asset/images/icons_map_carrier.png'
    });

    //extend the bounds to include each marker's position
    bounds.extend(marker.position);

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
        scrollAtas();
      }

    })(marker, i));
    markers.push(marker);
  }

  //now fit the map to the newly inclusive bounds
  map.fitBounds(bounds);
  // alert(map.getZoom());
  // if (locations.length == 1) {
    zoomChangeBoundsListener = 
        google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
          // alert(this.getZoom());
            if (this.getZoom() > 16){
                this.setZoom(16);
            }
    });
    setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 2000);
  // }
  //(optional) restore the zoom level after the map is done scaling
  // var listener = google.maps.event.addListener(map, "idle", function () {
  //     map.setZoom(15);
  //     google.maps.event.removeListener(listener);
  // });
}
function myClick(id){
    google.maps.event.trigger(markers[id], 'click');
}
function scrollAtas() {
  var target = $('#map');
  $('body,html').animate({
    scrollTop: target.offset().top
  }, 800);
}
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM1PImx-AO3jyoGmS9wzpW59SIzR-cMHU&callback=initMap">
</script>
