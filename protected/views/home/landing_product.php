<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>products</h2>
        <h4>THE BEST AT ITS&rsquo;<br>class</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct landing_prd">

          <div class="row">
            <div class="col-md-3 col-sm-3">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
                  <div class="list">
                    <a class="top" href="#">Commercial</a>
                    <ul class="list-unstyled">
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Air Handler</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Chillers</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Fan Coil</a></li>
                      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Package Unit</a></li>
                    </ul>
                    <div class="clear"></div>
                  </div>
                  <div class="list">
                    <a class="top" href="#">Low Commercial</a>
                    <ul class="list-unstyled">
                      <li><a href="#">Light Commercial</a></li>
                      <li><a href="#">Residential</a></li>
                      <li><a href="#">VRF</a></li>
                    </ul>
                    <div class="clear"></div>
                  </div>

                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            <div class="col-md-9 col-sm-9">
              <div class="rights_cont">
                <h6>Select Category</h6>
                <div class="clear height-5"></div>

                <div class="lists_bloc_landing_prdItems">
                  <div class="items">
                    <h3 class="subs_titles">Commercial</h3>
                    <div class="clear height-20"></div>
                    <div class="subs_list_landing">
                      <div class="row">
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-1.jpg" alt="" class="img-responsive center-block"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Air handler</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-2.jpg" alt="" class="img-responsive"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Chillers</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-3.jpg" alt="" class="img-responsive"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Fan Coil</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-4.jpg" alt="" class="img-responsive"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Package Unit</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="clear"></div>
                  </div>

                  <div class="items">
                    <h3 class="subs_titles">Low Commercial</h3>
                    <div class="clear height-20"></div>
                    <div class="subs_list_landing">
                      <div class="row">
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-1.jpg" alt="" class="img-responsive center-block"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Light Commercial</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-2.jpg" alt="" class="img-responsive"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Residential</a>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><img src="<?php echo $this->assetBaseurl; ?>pict-landingprd-3.jpg" alt="" class="img-responsive"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">VRF</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="clear"></div>
                  </div>

                </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>
