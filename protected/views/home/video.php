<?php 
$data_event = array(
      1 => array(
            'title'=>'Start-up services & extended warranty protection',
            'short_desc'=>'<p>Carrier commercial equipment represents today&rsquo;s most advanced technology. To ensure that you receive the full benefits from this leading edge design, proper start-up, following a rigorous, factory-defined set of procedures is essential. Correct start-up is the key to optimum performance, safety and reliability, not just during the first days or weeks of operation, but for years to come.</p>',
            'desc'=>'',
            ),

  );
?>
<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_video">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>VIDEOS</h2>
        <h4>UNDERSTAND BETTER<br>THROUGH OUR VIDEOS</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices conts_pVideo">

          <div class="row">
            <div class="col-md-12">
              <div class="rights_cont full_c">
                <!-- <h6>4 events in May 2017</h6> -->
                <div class="clear height-0"></div>

                <!-- Start default list data -->
                <div class="lists_data_defaults_lh videos">
                  <div class="row default">
                  <?php /* for ($i=13; $i >= 9; $i--) { ?>
                  <div class="col-md-15 col-sm-4 col-xs-6">
                    <div class="items">
                      <div class="picture prelatife">
                        <!-- <img src="<?php // echo $this->assetBaseurl ?>pict_videos_ex.jpg" alt="" class="img-responsive"> -->
                        <iframe width="252" height="143" src="https://www.youtube.com/embed/XYq6e4dMQrA" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="info">
                        <span class="dates"><?php echo $i ?> Mei 2017</span>
                        <h6>Carrier SMART Service for Commercial Air Conditioning Systems - HVAC</h6>
                        <p>Carrier SMART Service for Commercial Air Conditioning Systems - HVAC. Commercial HVAC system owners now have a new ...</p>
                      </div>
                    </div>
                  </div>
                  <?php } */ ?>
                  <?php foreach ($dataVideo->getData() as $key => $value): ?>
                  <div class="col-md-15 col-sm-4 col-xs-6">
                    <div class="items">
                      <div class="picture prelatife">
                        <!-- <iframe width="282" height="143" src="https://www.youtube.com/embed/<?php // echo Common::getVYoutube($value->url_youtube) ?>" frameborder="0" allowfullscreen></iframe> -->
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo Common::getVYoutube($value->url_youtube) ?>' frameborder='0' allowfullscreen></iframe></div>
                      </div>
                      <div class="info">
                        <span class="dates"><?php echo date('d F Y', strtotime($value->date)) ?></span>
                        <h6><?php echo $value->description->title ?></h6>
                        <p><?php echo $value->description->content ?></p>
                      </div>
                    </div>
                  </div>
                  <?php endforeach ?>
                  
                  </div>
                  <div class="clear"></div>
                </div>
                <!-- End default list data -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>
