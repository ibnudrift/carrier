<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_contact">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['contact_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['contact_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pContact">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
              <?php echo $this->setting['contact_left'] ?>
              <?php /*
                <h5>JAKARTA HEADQUARTER</h5>
                <div class="clear height-15"></div>
                <p><strong>Headquarter.</strong><br />
                PT. BERCA CARRIER INDONESIA<br />
                GPN JIEXPO Lt.4<br />
                Jl. Benyamin Sueb - Kemayoran<br />
                Jakarta - DKI Jakarta, Indonesia</p>

                <p><strong>Phone.</strong><br />
                (021) 26645888</p>

                <p><strong>Email.</strong><br />
                <a href="mailto:mailto:info@carrier.co.id" target="_blank">info@carrier.co.id</a></p>
              */ ?>
              </div>
            </div>
            <div class="col-md-9">
              <div class="rights_cont">
                <?php echo $this->setting['contact_content'] ?>
                <div class="clear height-35"></div>
                <?php echo $this->renderPartial('//home/_form_contact2', array('model'=>$model)); ?>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>
