<section class="default_sc top_inside_pg_default">
  <div class="out_table">
    <div class="in_table">
      <div class="prelatife container">
        <h1 class="sub_titlepage">Berita & Artikel</h1>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section class="default_sc insides_middleDefaultpages back-white">
  <div class="prelatife container">
    <div class="clear height-50"></div><div class="height-5"></div>
    <div class="content-text text-center">
      
      <div class="outers_listing_newshome defaults_t">
            <div class="row default">
                <?php foreach ($dataBlog->getData() as $key => $value): ?>
                    <div class="col-md-4 col-sm-4">
                      <div class="items">
                          <div class="pict"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(313,204, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></a></div>
                          <div class="desc">
                              <div class="titles"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><?php echo $value->description->title ?></a></div>
                              <div class="clear"></div>
                              <span class="dates"><?php echo date('d F Y', strtotime($value->date_input)) ?></span>
                              <div class="clear"></div>
                              <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>" class="btn btn-default btns_news_default">BACA</a>
                          </div>
                      </div>
                    </div>
                <?php endforeach ?>
            </div>
            <div class="clear"></div>
        </div>
        <!-- end listing news -->
        <div class="clear height-10"></div>
      <div class="clear"></div>
    </div>
    <!-- end content berita artikel -->
    <div class="text-center bgs_paginations">
          <?php $this->widget('CLinkPager', array(
              'pages' => $dataBlog->getPagination(),
              'header' => '',
          )) ?>
    </div>
    <div class="clear height-20"></div>

    <div class="clear"></div>
  </div>
</section>

