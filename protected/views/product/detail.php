<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>products</h2>
        <h4>THE BEST AT ITS&rsquo;<br>class</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-3 hide hidden">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
        <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                  <div class="list">
                    <a class="top" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                    <ul class="list-unstyled">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li <?php if ($v->id == $data->category_id): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                    <div class="clear"></div>
                  </div>
        <?php endforeach ?>

                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            <div class="col-md-12 outers_blockDetail_new">
              <div class="rights_cont block_detail_products">
                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$category->id)); ?>" class="btn btn-link btnsr_back_product"><i class="fa fa-arrow-left"></i> &nbsp;BACK TO <?php echo strtoupper($category->description->name) ?> PRODUCTS</a>
                <div class="clear height-20"></div>

                <div class="row">
                  <div class="col-md-5 col-sm-6">
                    <div class="pictures_box">
                      <div class="pict_big"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(496,496, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive full_img"></div>
                      <div class="list_chiild">
                        <ul class="list-inline">
                          <li><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(120,120, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-6">
                    <div class="description">
                      <h4><?php echo $data->description->name ?></h4>
                      <div class="clear height-10"></div>
                      <h6><?php echo nl2br($data->description->subtitle) ?></h6>
                      <div class="clear height-10"></div>
                      <?php echo $data->description->desc ?>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>

                <div class="description pl-0">
                <div class="bottoms_desc">
                          <ul class="nav nav-tabs" role="tablist">
                            <?php if ($data->description->features != ''): ?>
                            <li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">FEATURES</a></li>
                            <?php endif ?>
                            <?php if ($data->description->options != ''): ?>
                            <li role="presentation"><a href="#option" aria-controls="option" role="tab" data-toggle="tab">OPTIONS</a></li>
                            <?php endif ?>
                            <?php if ($data->description->specifications != ''): ?>
                            <li role="presentation"><a href="#spesific" aria-controls="spesific" role="tab" data-toggle="tab">SPECIFICATIONS</a></li>
                            <?php endif ?>
                            <?php if ($data->description->brochures != ''): ?>
                            <li role="presentation"><a href="#brochures" aria-controls="brochures" role="tab" data-toggle="tab">BROCHURES</a></li>
                            <?php endif ?>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <?php if ($data->description->features != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="feature">
                              <?php echo $data->description->features ?>
                              <div class="clear"></div>
                            </div>
                            <?php endif ?>
                            <?php if ($data->description->options != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="option">
                              <?php echo $data->description->options ?>
                            </div>
                            <?php endif ?>
                            <?php if ($data->description->specifications != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="spesific">
                              <?php echo $data->description->specifications ?>
                            </div>
                            <?php endif ?>
                            <?php if ($data->description->brochures != ''): ?>
                            <div role="tabpanel" class="tab-pane" id="brochures">
                              <?php echo $data->description->brochures ?>
                            </div>
                            <?php endif ?>
                          </div>

                        
                          <div class="clear"></div>
                        </div>
<script type="text/javascript">
$(document).ready(function() {
  $('.bottoms_desc .nav.nav-tabs li:eq(0)').addClass('active');
  $('.bottoms_desc .tab-content .tab-pane:eq(0)').addClass('active');
})
</script>
                  <div class="clear"></div>
                </div>
                <!-- End bottm product Detail -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>