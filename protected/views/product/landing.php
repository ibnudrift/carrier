<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['product_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['product_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['product_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct landing_prd">

          <div class="row">
            <div class="col-md-3 col-sm-3">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
				<?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                  <div class="list">
                    <a class="top" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                    <ul class="list-unstyled">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li <?php if ($v->id == $_GET['category']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>

                    </ul>
                    <div class="clear"></div>
                  </div>
				<?php endforeach ?>

                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>


            <div class="col-md-9 col-sm-9">
              <div class="rights_cont">
                <h6>Select Category</h6>
                <div class="clear height-5"></div>
            <?php
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('t.parent_id = :parent_id');
            $criteria->params[':parent_id'] = 0;
            $criteria->addCondition('t.type = :type');
            $criteria->params[':type'] = 'category';
            if (isset($_GET['category'])) {
              $criteria->addCondition('t.id = :ids');
              $criteria->params[':ids'] = $_GET['category'];
            }
            $criteria->addCondition('description.language_id = :language_id');
            $criteria->params[':language_id'] = $this->languageID;
            // $criteria->limit = 3;
            $criteria->order = 'sort ASC';
            $subCategory = PrdCategory::model()->findAll($criteria);
            ?>
                <div class="lists_bloc_landing_prdItems">
        <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);
?>

                  <div class="items">
                    <h3 class="subs_titles"><?php echo $value->description->name ?></h3>
                    <div class="clear height-20"></div>
                    <div class="subs_list_landing">
                      <div class="row">
                      	<?php foreach ($subCategory2 as $k => $v): ?>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/category/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a>
                            </div>
                          </div>
                        </div>
                      	<?php endforeach ?>
                      </div>
                    </div>

                    <div class="clear"></div>
                  </div>
				<?php endforeach ?>


                </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>





