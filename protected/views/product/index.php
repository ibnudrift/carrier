<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(890,275, '/images/static/'.$this->setting['product_banner_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2><?php echo $this->setting['product_banner_title'] ?></h2>
        <h4><?php echo nl2br($this->setting['product_banner_subtitle']) ?></h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-3 hide hidden">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
        <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                  <div class="list">
                    <a class="top" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                    <ul class="list-unstyled">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li <?php if ($v->id == $_GET['category']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                    <div class="clear"></div>
                  </div>
        <?php endforeach ?>
                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            <!-- <div class="col-md-9"> -->

            <div class="col-md-12">
              <div class="rights_cont" style="padding-left: 0px; border-left: 0px;">
                <?php if ($strCategory != null): ?>
                  <?php if ($_GET['q'] != ''): ?>
                  <h6 class="sub_title">Search "<?php echo $_GET['q'] ?>" in <?php echo $strCategory->description->name ?><?php if ($strParentCategory != null): ?> - <?php echo $strParentCategory->description->name ?><?php endif ?></h6>
                  <?php else: ?>
                  <h6 class="sub_title s_breadcrumb"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a> &nbsp;>&nbsp; <?php if ($strParentCategory != null): ?><a href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$strParentCategory->id)); ?>"><?php echo $strParentCategory->description->name ?></a> &nbsp;>&nbsp; <?php endif ?><?php echo $strCategory->description->name ?></h6>
                  <?php endif ?>
                <?php elseif($_GET['q'] != ''): ?>
                <h6 class="sub_title">Cari "<?php echo $_GET['q'] ?>" di kategori <?php echo $strCategory->description->name ?><?php if ($strParentCategory != null): ?> - <?php echo $strParentCategory->description->name ?><?php endif ?></h6>
                <?php else: ?>
                <h6 class="sub_title">Cari Semua Produk</h6>
                <?php endif ?>
                <!-- <h6>COMMERCIAL &nbsp;>&nbsp; AIR HANDLERS</h6> -->
                <div class="clear height-5"></div>
                   <?php
                    $criteria=new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('description.language_id = :language_id');
                    $criteria->params[':language_id'] = $this->languageID;
                    $criteria->addCondition('topik_id = :type');
                    $criteria->params[':type'] = $strCategory->id;
                    $criteria->group = 't.id';
                    $criteria->order = 't.id ASC';
                    $slide = Slide::model()->with(array('description'))->findAll($criteria);
                    ?>
                    <?php if ($slide): ?>
                <div class="tops_landing_products">
                    <div id="carousel-exn_banner" class="carousel fade" data-ride="carousel" data-interval="3500">
                      <ol class="carousel-indicators">
                        <?php foreach($slide as $key => $value): ?>
                        <li data-target="#carousel-exn_banner" data-slide-to="<?php echo $key ?>" <?php echo ($key == 0)? 'class="active"':''; ?>></li>
                        <?php endforeach ?>
                      </ol>
                      <div class="carousel-inner" role="listbox">
                        <?php foreach($slide as $key => $value): ?>
                        <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                          <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1600,575, '/images/slide/'.$value->image, array('method' => 'resize', 'quality' => '90')) ?>" alt="<?php echo $strCategory->description->name ?>">
                        </div>
                        <?php endforeach ?>
                      </div>
                    </div>
                    <!-- end slide banner -->
                </div>
                    <?php endif ?>
                <div class="clear"></div>
                <?php /*if (isset($_GET['category']) AND $_GET['category'] == 7): ?>
                  <!-- chiller -->
                  <div class="tops_landing_products">
                    <div class="info">
                      <h5>ONE COMPANY. TONS OF CAPACITY.</h5>
                      <p><b>With a wide range of types, capacities and sustainable refrigerant options, Carrier is a leader in chiller technology. In fact, we always have been.</b></p>
                      <p>From the invention of the first centrifugal chiller in 1922 to today&rsquo;s AquaEdge&trade; 23XRV &ndash; the world&rsquo;s leading efficiency screw chiller &ndash; Carrier has consistently led the way in efficient, reliable chiller technology.</p>
                      <div class="clear"></div>
                    </div>
                    <div class="block_pict chiller"></div>
                  </div>
                <?php elseif ($_GET['category'] == 8): ?>
                  <!-- fan coil -->
                  <div class="tops_landing_products">
                    <div class="info">
                      <h5>SIMPLE. QUIET. CONTROL.</h5>
                      <p><b>At Carrier, we&rsquo;re quietly improving efficiency and bringing flexible control to fan coils.</b></p>
                      <p>With floor, ceiling and furred-in units, Carrier gives you options for improving efficiency and reducing noise – all with easy operation and the durability of copper tubes with aluminum fins.</p>
                      <div class="clear"></div>
                    </div>
                    <div class="block_pict fan_coil"></div>
                  </div>
                <?php elseif ($_GET['category'] == 9): ?>
                  <!-- fan coil -->
                  <div class="tops_landing_products">
                    <div class="info">
                      <h5>CARRIER DELIVERS EFFICIENT, DEPENDABLE PERFORMANCE, INSIDE AND OUT</h5>
                      <p>With Carrier split systems, a wide range of outdoor condensing units work seamlessly with innovative indoor air handlers to create reliable solutions that are easy-to-install and service for a wide range of commercial HVAC needs.</p>
                      <div class="clear"></div>
                    </div>
                    <div class="block_pict package"></div>
                  </div>
                <?php elseif ($_GET['category'] == 6): ?>
                  <!-- fan coil -->
                  <div class="tops_landing_products">
                     <div class="info">
                      <h5>NO MATTER THE APPLICATION, CARRIER CAN HANDLE IT.</h5>
                      <p><b>Carrier offers a range of air-handling units to meet the demands of precise indoor environments.</b></p>
                      <p>Whether you need a custom-built or preconfigured product, Carrier air handlers are available with a wide airflow range, flexible designs and advanced filtration options to bring clean air and comfort to a variety of spaces.</p>
                      <div class="clear"></div>
                    </div>
                    <div class="block_pict"></div>
                  </div>
                <?php else: ?>
                  <div class="tops_landing_products">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/category/<?php echo $strCategory->image2 ?>" alt="<?php echo $strParentCategory->description->name ?> - <?php echo $strCategory->description->name ?>" class="img-responsive">
                  </div>
                <?php endif;*/ ?>

                <div class="middles_products">
                  <div class="clear height-35"></div>
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                      <!-- Start Left c -->
                      <?php // echo $this->renderPartial('//home/_left_productCat', array('category_id' => $strCategory->id)); ?>
<?php
$get = array();
if ($_GET['category'] != '') {
  $get['category'] = $_GET['category'];
}
?>
                      <form action="<?php echo $this->createUrl('/product/index', $get) ?>" method="get" id="form-filter">
                      <div class="lefts_c">
                        <h6>REFINE BY</h6>
                        <div class="filtering_data">
                        <?php foreach ($typeLabel as $key => $value): ?>
                          
                          <div class="list_filter">
                            <p><b><?php echo $value['data']->name ?></b></p>
                            <?php foreach ($value['child'] as $val): ?>
                            <?php if ($val): ?>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="select-filter" name="<?php echo urlencode($value['data']->name) ?>[]" value="<?php echo urlencode($val) ?>"  <?php if (isset($_GET[urlencode($value['data']->name)]) AND in_array(urlencode($val), $_GET[urlencode($value['data']->name)])): ?>checked<?php endif ?>>
                                <?php echo $val ?> 
                              </label>
                            </div>
                            <?php endif ?>
                            <?php endforeach ?>

                            <div class="clear"></div>
                          </div>
                        <?php endforeach ?>
                        </div>

                      </div>
                      </form>
                      <!-- End Left c -->
                    </div>

                    <div class="col-md-9 col-sm-9">
                      <div class="rights_c">
                          <h6><?php echo $product->getTotalItemCount(); ?> PRODUCTS</h6>
<?php
$data = $product->getData();
?>
                <?php if ($product->getTotalItemCount() > 0): ?>
<?php
$filterHeader = PrdProductFilter::model()->findAll('product_id = :id ORDER BY `name` ASC', array(':id'=>$data[0]->id));
$headerFilter = array();
foreach ($filterHeader as $key => $value) {
  $headerFilter[] = trim($value->name);
}
?>

                        <div class="lists_sub_products">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>Model</th>
                                  <?php foreach ($headerFilter as $key => $value): ?>
                                  <th><?php echo $value ?></th>
                                  <?php endforeach ?>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($data as $key => $value): ?>
<?php
$filterHeader = PrdProductFilter::model()->findAll('product_id = :id', array(':id'=>$value->id));
$filterValue = array();
foreach ($filterHeader as $v) {
  $filterValue[trim($v->name)] = $v->value;
}
?>
                                <tr>
                                  <td>
                                  <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)); ?>">
                                    <span class="nmes"><?php echo $value->description->name ?></span>
                                    <div class="clear"></div>
                                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(135,135, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive">
                                    </a>
                                  </td>
                                  <?php foreach ($headerFilter as $v): ?>
                                  <td><?php echo $filterValue[$v] ?></td>
                                  <?php endforeach ?>
                                </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                            <div class="clear height-25"></div>
                            <?php $this->widget('CLinkPager', array(
                                'pages' => $product->getPagination(),
                                'header' => '',
                                'htmlOptions' => array('class'=>'pagination'),
                                'selectedPageCssClass' => 'active',
                            )) ?>
                            <div class="clear"></div>
                        </div>
                        <!-- End list sub products -->
                <?php else: ?>
                <h3 class="text-center">No Data</h3>
                <?php endif ?>

                        <div class="clear"></div>
                      </div>
                      <!-- End Right c -->
                    </div>
                  </div>

                  <div class="clear"></div>
                </div>

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>




<script type="text/javascript">
$('.select-filter').on('change', function() {
  $('#form-filter').submit();
})
</script>






