<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>products</h2>
        <h4>THE BEST AT ITS&rsquo;<br>class</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
        <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                  <div class="list">
                    <a class="top" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                    <ul class="list-unstyled">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li <?php if ($v->id == $data->category_id): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                    <div class="clear"></div>
                  </div>
        <?php endforeach ?>

                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            
            <div class="col-md-9 outers_blockDetail_new2">
              <div class="rights_cont block_detail_products">
                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$category->id)); ?>" class="btn btn-link btnsr_back_product"><i class="fa fa-arrow-left"></i> &nbsp;BACK TO <?php echo strtoupper($category->description->name) ?> PRODUCTS</a>
                <div class="clear height-20"></div>

                <div class="row">
                  <div class="col-md-6 col-sm-6 hidden hide">
                    <div class="pictures_box">
                      <div class="pict_big"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(496,496, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                      <div class="list_chiild">
                        <ul class="list-inline">
                          <li><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(120,120, '/images/product/'.$data->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <div class="description">
                      <h4><?php echo $data->description->name ?></h4>
                      <div class="clear height-10"></div>
                      <h6><?php echo nl2br($data->description->subtitle) ?></h6>
                      <div class="clear height-10"></div>
                      <?php // echo $data->description->desc ?>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>

                <div class="description pl-0">

                <div class="bottoms_desc pt-0">
                  <?php if ($data->description->features != ''): ?>
                  <div class="blocks_list padding-bottom-30"><?php echo $data->description->features ?></div>
                  <?php endif ?>
                  <?php if ($data->description->desc != ''): ?>
                  <div class="blocks_list padding-bottom-30"><?php echo $data->description->desc ?></div>
                  <?php endif ?>

                  <?php /*if ($data->description->specifications != ''): ?>
                  <div class="blocks_list padding-bottom-30"><?php echo $data->description->specifications ?></div>
                  <?php endif;*/ ?>
                  <?php /*if($data->description->options != ''): ?>
                  <div class="blocks_list padding-bottom-30"><?php echo $data->description->options ?></div>
                  <?php endif;*/ ?>
                  <?php if ($data->description->brochures != ''): ?>
                  <div class="blocks_list padding-bottom-30"><?php echo $data->description->brochures ?></div>
                  <?php endif ?>
                </div>

                <?php if ($data->type == 1): ?>
                    <div class="clear height-30"></div>
                    <div class="row blocks_bottomDetail_pict_type1">
                      <div class="col-md-6 col-sm-6">
                        <div class="blocks_leftn_cbutton padding-top-50 padding-bottom-50 text-center">
                          <div class="blocks_ls">
                            <div class="list padding-bottom-5">
                              <a href="javascript:return false;" data-toggle="modal" data-target="#myModal_spesifications"><img src="<?php echo $this->assetBaseurl ?>btns_spec_prd-child.jpg" alt="button view spesification" class="img-responsive center-block"></a>
                            </div>
                            <div class="list padding-bottom-5">
                              <a href="javascript:return false;" data-toggle="modal" data-target="#myModal_remote"><img src="<?php echo $this->assetBaseurl ?>btns_remote_prd-child.jpg" alt="button view spesification" class="img-responsive center-block"></a>
                            </div>
                            <?php if ($data->file): ?>
                            <div class="list padding-bottom-5">
                              <a href="<?php echo Yii::app()->baseUrl.'/images/file/'. $data->file; ?>" target="_blank"><img src="<?php echo $this->assetBaseurl ?>btns_brochure_prd-child.jpg" alt="button view spesification" class="img-responsive center-block"></a>
                            </div>
                            <?php endif ?>
                          </div>
                        </div>
                        <!-- End blocks ls -->
                      </div>
                      <div class="col-md-6 col-sm-6">
                        <div class="pictures_box">
                          <div class="pict_big"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(538,1000, '/images/product/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
                        </div>
                        <!-- end left cont box -->
                      </div>
                    </div>
                    <?php endif ?>
                <?php /*
                <div class="bottoms_desc hide hidden">
                    <ul class="nav nav-tabs" role="tablist">
                      <?php if ($data->description->features != ''): ?>
                      <li role="presentation"><a href="#feature" aria-controls="feature" role="tab" data-toggle="tab">FEATURES</a></li>
                      <?php endif ?>
                      <?php if ($data->description->options != ''): ?>
                      <li role="presentation"><a href="#option" aria-controls="option" role="tab" data-toggle="tab">OPTIONS</a></li>
                      <?php endif ?>
                      <?php if ($data->description->specifications != ''): ?>
                      <li role="presentation"><a href="#spesific" aria-controls="spesific" role="tab" data-toggle="tab">SPECIFICATIONS</a></li>
                      <?php endif ?>
                      <?php if ($data->description->brochures != ''): ?>
                      <li role="presentation"><a href="#brochures" aria-controls="brochures" role="tab" data-toggle="tab">BROCHURES</a></li>
                      <?php endif ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <?php if ($data->description->desc != ''): ?>
                      <div role="tabpanel" class="tab-pane active" id="desc">
                        <h6>DESCRIPTION</h6>
                        <?php echo $data->description->desc ?>
                        <div class="clear"></div>
                      </div>
                      <?php endif ?>

                      <?php if ($data->description->features != ''): ?>
                      <div role="tabpanel" class="tab-pane active" id="feature">
                        <h6>FEATURES</h6>
                        <?php echo $data->description->features ?>
                        <div class="clear"></div>
                      </div>
                      <?php endif ?>
                      <?php if ($data->description->options != ''): ?>
                      <div role="tabpanel" class="tab-pane active" id="option">
                        <h6>OPTIONS</h6>
                        <?php echo $data->description->options ?>
                      </div>
                      <?php endif ?>
                      <?php if ($data->description->specifications != ''): ?>
                      <div role="tabpanel" class="tab-pane active" id="spesific">
                        <h6>SPESIFICATION</h6>
                        <?php echo $data->description->specifications ?>
                      </div>
                      <?php endif ?>
                      <?php if ($data->description->brochures != ''): ?>
                      <div role="tabpanel" class="tab-pane active" id="brochures">
                        <h6>REMOTE CONTROL</h6>
                        <?php echo $data->description->brochures ?>
                        <!-- <div class="down_brochure">
                          <div class="lists"><i class="fa-brochure"></i> 39CQ PRODUCT INFORMATION BROCHURE</div>
                          <div class="lists"><i class="fa-brochure"></i> 39CQ PRODUCT INFORMATION BROCHURE</div>
                        </div> -->
                      </div>
                      <?php endif ?>
                    </div>
                    <!-- End bottom desc -->
                  </div>
                        <script type="text/javascript">
                        $(document).ready(function() {
                          // $('.bottoms_desc .nav.nav-tabs li:eq(0)').addClass('active');
                          // $('.bottoms_desc .tab-content .tab-pane:eq(0)').addClass('active');
                        })
                        </script>
                        */ ?>
                  <div class="clear"></div>
                </div>
                <!-- End bottm product Detail -->

                <div class="clear"></div>
              </div>
              <!-- End rights content -->

            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>

<style type="text/css" media="screen">
  /*.tab-pane{
    border-bottom: 1px solid #ccc;
    margin-bottom: 1.9em;
    padding-bottom: 0.5em;
  }
  .tab-pane:last-child{
    border-bottom: 0px;
    margin-bottom: 0em;
  }*/
</style>


<!-- Modal -->
<div class="modal fade" id="myModal_spesifications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><img src="<?php echo $this->assetBaseurl ?>lgo-headers_carrier_res.png" alt="" class="img-responsive"></h4>
      </div>
      <div class="modal-body">
        <h3>Spesifications</h3>
        <?php if ($data->description->specifications != ''): ?>
        <div class="blocks_list padding-bottom-10"><?php echo $data->description->specifications ?></div>
        <?php endif ?>
        <style type="text/css">
          .modal-body h3{ margin: 0; text-align: center; line-height: 1; margin-bottom: 20px; }
          .modal-body .blocks_list img{ max-width: 100%;  }
          .modal-body .blocks_list p{ margin-bottom: 0; }
        </style>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal_remote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><img src="<?php echo $this->assetBaseurl ?>lgo-headers_carrier_res.png" alt="" class="img-responsive"></h4>
      </div>
      <div class="modal-body">
        <h3>Remote Controller</h3>
        <?php if ($data->description->options != ''): ?>
        <div class="blocks_list padding-bottom-10"><?php echo $data->description->options ?></div>
        <?php endif ?>
        <style type="text/css">
          .modal-body h3{ margin: 0; text-align: center; line-height: 1; margin-bottom: 20px; }
          .modal-body .blocks_list img{ max-width: 100%;  }
          .modal-body .blocks_list p{ margin-bottom: 0; }
        </style>
      </div>
    </div>
  </div>
</div>
