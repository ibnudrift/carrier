<section class="middles-content backs_covr_checkgenuine">
	
	<?php
	$repeat_max = intval($this->setting['nilai_sebanyak']);
	?>
	<div class="inners_section">
		<?php if ($data['false_search'] === false && $model->terpakai < $repeat_max): ?>
			<h2 style="text-transform: uppercase;"><b>Congratulation!</b></h2>
			<p>You have purchased Carrier Genuine Part</p>
		<?php else: ?>
			<h2><b>VERIFICATION</b><br>
				GENUINE PART CARRIER</h2>
			<p>Please input the unique code printed on hologram sticker <br>
			(hologram sticker is attached on the packaging material)</p>
		<?php endif ?>
		<div class="ins_form">
		<form class="Checkpart_form" action="" method="POST">
			<div class="form-group bx_input <?php echo ($data['false_search'] === false && $model->terpakai < $repeat_max) ? 'success': 'warning'  ?>">
				<input type="text" name="code" value="<?php echo $data['code'] ?>" class="form-control">
				<?php if (!isset($data['code'])): ?>
				<button type="submit" class="btn">OK</button>
				<?php endif; ?>

				<?php if (isset($data['code'])): ?>
				<div class="sx_icon">
					<?php if ($data['false_search'] === false && $model->terpakai < $repeat_max): ?>
					<span class="successl"></span>
					<?php else: ?>
					<span class="closetimes"></span>
					<?php endif; ?>
				</div>
				<?php endif ?>
			</div>
			<?php if ($data['false_search'] === true AND isset($data['code'])): ?>
			<p class="warning"><?php echo $this->setting['customtext_gen_false'] ?></p>
			<?php endif ?>

			<?php if ($data['false_search'] === false): ?>
				<?php if ($model->terpakai >= $repeat_max): ?>
				<p class="warning"><?php echo $this->setting['customtext_gen_false'] ?></p>	
				<?php else: ?>
					<?php if (intval($model->terpakai) < 2): ?>
					<!-- <p class="success"><?php // echo $this->setting['customtext_gen_true'] ?></p>	 -->
					<?php else: ?>
					<?php $model->terpakai = $model->terpakai - 1; ?>
					<p class="success"><?php echo $this->setting['customtext_gen_true'] ?> <span class="red"><?php echo $model->terpakai ?> times</span></p>
					<?php endif ?>
				<?php endif ?>
			<?php endif ?>

			<?php if (isset($data['code'])): ?>
			<div class="clear height-10"></div>
			<div class="text-center">
				<a href="<?php echo CHtml::normalizeUrl(array('/product/checkpart')); ?>" class="btn btn-link p-0">Check another Code</a>
			</div>
			<?php endif ?>

		</form>
		</div>
		<div class="btm_forms_dta">
			<p>For more information, you can contact us in</p>
			<div class="ndx_blocks">
				<div class="items_lin">
					<div class="d-inline dn_icon">
						<img src="<?php echo $this->assetBaseurl ?>small-icons-tn1.png" alt="" class="img-responsive">
					</div>
					<div class="d-inline dn_texts">
						<p>Call Center:<br><a href="tel:02126608088">021-2660-8088</a></p>
					</div>
				</div>
				<div class="items_lin">
					<div class="d-inline dn_icon">
						<img src="<?php echo $this->assetBaseurl ?>small-icons-tn2.png" alt="" class="img-responsive">
					</div>
					<div class="d-inline dn_texts">
						<p>Email:<br><a href="mailto:part.info@carrier.co.id">part.info@carrier.co.id</a></p>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clearfix"></div>
		</div>

		<div class="clear"></div>
	</div>

	<div class="clear"></div>
</section>

<?php 
$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
?>
<script type="text/javascript">
	
	if ( window.history.replaceState ) {
	  window.history.replaceState( null, null, window.location.href );
	  // window.history.replaceState( null, null, <?php // echo $baseUrl.'/verify_genuine' ?> );
	}

	function findGetParameter(parameterName) {
	    var result = null,
	        tmp = [];
	    location.search
	        .substr(1)
	        .split("&")
	        .forEach(function (item) {
	          tmp = item.split("=");
	          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	        });
	    return result;
	}
	var params_code    = findGetParameter('code');
	var params_stcheck = findGetParameter('stcheck');

	if (parseInt(params_stcheck) == 1) {
		$('.Checkpart_form input').val(params_code);

		$('<input>').attr({
		    type: 'hidden',
		    class: 'form-control',
		    value: '1',
		    name: 'from_excel'
		}).appendTo('form.Checkpart_form');

		<?php if ($data['repeat_str'] === false) {  ?>
		 	$('.Checkpart_form').submit();
		<?php } ?>
	}

</script>
