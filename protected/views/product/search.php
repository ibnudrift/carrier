<div class="outer_subpage_wrapper">
  <div class="subpage_top_banner_illustration pg_products">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-45"></div>
      <div class="info padding-left-25">
        <h2>products</h2>
        <h4>THE BEST AT IT&rsquo;S<br>class</h4>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage illustration -->

  <div class="middles_cont back-white">

    <section class="middle_conts_1_inside">
      <div class="prelatife container">
        <div class="inside content-text padding-left-25 conts_pServices cont_pProduct">

          <div class="row">
            <div class="col-md-3">
              <div class="lefts">
                <h5>CATEGORY</h5>
                <div class="clear height-15"></div>
                <div class="blocsl_lmenu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
        <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);

?>
                  <div class="list">
                    <a class="top" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                    <ul class="list-unstyled">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li <?php if ($v->id == $_GET['category']): ?>class="active"<?php endif ?>><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                    <div class="clear"></div>
                  </div>
        <?php endforeach ?>
                </div>
                <!-- end left menu -->

                <div class="celar"></div>
              </div>
            </div>
            <div class="col-md-9 pg_searchs_product">
              <div class="rights_cont">
                <?php
                $data = $product->getData();
                ?>
                <?php if ($product->getTotalItemCount() > 0): ?>
               <div class="lists_bloc_landing_prdItems">
                  <div class="items">
                    <h3 class="subs_titles">Searching data product for "<?php echo $_GET['q'] ?>"</h3>
                    <div class="clear height-20"></div>
                    <div class="subs_list_landing">
                      <div class="row">
                        <?php foreach ($data as $k => $v): ?>
                        <div class="col-md-4 col-sm-6">
                          <div class="item">
                            <div class="picture">
                              <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$v->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(210,210, '/images/product/'.$v->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></a>
                            </div>
                            <div class="info">
                              <a href="<?php echo CHtml::normalizeUrl(array('/product/detail', 'id'=>$v->id)); ?>"><?php echo $v->description->name ?></a>
                            </div>
                          </div>
                        </div>
                        <?php endforeach ?>
                      </div>
                    </div>
                    <?php $this->widget('CLinkPager', array(
                        'pages' => $product->getPagination(),
                        'header' => '',
                        'htmlOptions' => array('class'=>'pagination'),
                        'selectedPageCssClass' => 'active',
                    )) ?>

                    <div class="clear"></div>
                  </div>

                </div>
                <?php endif; ?>
                <div class="clear"></div>
              </div>
            </div>
          </div>

          <div class="clear height-25"></div>
        </div>
      </div>
    </section>

    <!-- End middle conts -->
  </div>

  <div class="clear"></div>
</div>

<div class="blocks_spn_backtops">
  <a href="#" class="btn btn-link btns_to_top">BACK TO TOP &nbsp;<i class="fa fa-chevron-up"></i></a>
</div>




<script type="text/javascript">
$('.select-filter').on('change', function() {
  $('#form-filter').submit();
})
</script>






