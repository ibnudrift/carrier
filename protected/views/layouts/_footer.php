<footer class="foot">
	<div class="prelatife container_footer"> 
		<div class="insides_footer">
			<div class="tops_footer">
				<div class="row">
					<div class="col-md-12">
						<div class="blk_menuFooter">
							<div class="row default">
								<div class="col-md-15">
									<div class="lgo_footers"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo_footers_dtn-l.jpg" alt="" class="img-responsive"></a></div>
								</div>
								<div class="col-md-15 col-sm-6">
									<h6>PRODUCTS</h6>
									<?php
										$criteria = new CDbCriteria;
										$criteria->with = array('description');
										$criteria->addCondition('t.parent_id = :parent_id');
										$criteria->params[':parent_id'] = 0;
										$criteria->addCondition('t.type = :type');
										$criteria->params[':type'] = 'category';
										$criteria->addCondition('description.language_id = :language_id');
										$criteria->params[':language_id'] = $this->languageID;
										// $criteria->limit = 2;
										$criteria->order = 'sort ASC';
										$subCategory = PrdCategory::model()->findAll($criteria);

										?>
									<!-- <ul class="list-unstyled blocks_listprod"> -->
									<ul class="list-unstyled">
										<?php foreach ($subCategory as $key => $value): ?>
											<li class="dropdown">
						                      <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
						                    </li>
						                <?php endforeach; ?>
									</ul>
								</div>
								<div class="col-md-15 col-sm-6">
									<h6>SERVICES</h6>
									<ul class="list-unstyled">
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/services')); ?>">Service and Maintenance</a></li>
					                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'retrofit')); ?>">Service Retrofit/Optimize</a></li>
					                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'parts-center')); ?>">Parts Center</a></li>
					                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'warranty')); ?>">Warranty</a></li>
					                    <li><a href="<?php echo CHtml::normalizeUrl(array('/product/checkpart')); ?>">Genuine Part Verification</a></li>
									</ul>
								</div>
								<div class="col-md-15 col-sm-6">
									<h6>MORE ON CARRIER</h6>
									<ul class="list-unstyled">
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'dealer-location')); ?>">Find an expert</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/video')); ?>">Videos</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">Events</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
									</ul>
								</div>
								<div class="col-md-15 col-sm-6">
									<h6>PT. Berca Carrier Indonesia</h6>
									<!-- <p>Phone. (021) 266 45888<br>
									Email. <a href="mailto:info@carrier.co.id">info@carrier.co.id</a><br>
									Gedung Pusat Niaga 4th Floor<br>
									Arena PRJ Kemayoran<br>
									Jakarta 10620, Indonesia</p> -->
									<?php echo ($this->setting['contact_footer_info']); ?>
								</div>
							</div>
							<div class="clear height-35"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear height-30"></div>
			<div class="line_footer"></div>
			<div class="clear height-20"></div>

			<div class="row">
				<div class="col-md-5">
					<p class="t-copyrights">
						Copyright &copy; 2017 PT Berca Carrier Indonesia.<br>
						<small>Trademarks are proprietary to PT Berca Carrier Indonesia</small>
					</p>
				</div>
				<div class="col-md-7 text-right">
					<div class="d-inline padding-right-20 btms_mnfooter">
						<a href="#">Privacy notice</a>&nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="#">Terms of user</a>&nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="#">Terms & conditions of sale</a>&nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="#">Sitemap</a>
					</div>
					<div class="socmed_footer d-inline">
						<?php if ($this->setting['url_twitter'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_twitter'] ?>"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($this->setting['url_instagram'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($this->setting['url_facebook'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook-square"></i></a>&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($this->setting['url_linkedin'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_linkedin'] ?>"><i class="fa fa-linkedin"></i></a>&nbsp;&nbsp;&nbsp;
						<?php endif ?>
						<?php if ($this->setting['url_youtube'] != ''): ?>
						<a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>"><i class="fa fa-youtube"></i></a>
						<?php endif ?>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear height-15"></div>
		</div>
	</div>
</footer>
