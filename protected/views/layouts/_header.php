<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;

    $criteria = new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('parent_id = 0');
    $criteria->addCondition('type = "category"');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->params[':language_id'] = $this->languageID;
    $criteria->order = 'sort ASC';
    $dataCategory = PrdCategory::model()->findAll($criteria);
?>
<div class="outers_back-header">
<header class="head">
  <div class="visible-lg visible-md prelatife">
    <div class="afx_rights_headertop_fn">
      <!-- <span><?php // echo $this->setting['contact_float_phone'] ?></span>
      <small class="inf">Show/Hide</small> -->
      <a href="<?php echo CHtml::normalizeUrl(array('/product/checkpart')); ?>"><img src="<?php echo Yii::app()->baseUrl.'/asset/images/' ?>floats_yellow_toprghts.png" alt="" class="img-responsive"></a>
    </div>

    <div class="prelatife container z60">
      <div class="ins">
      <div class="tops">
        <div class="row">
          <div class="col-md-6">
            <div class="left_tp_header hide hidden">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'warranty')); ?>" class="btn btn-link backs_blue1 d-inline">WARRANTY REGISTRATION</a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'dealer')); ?>" class="btn btn-link backs_blue2 d-inline"><i class="fa fa-lock"></i> &nbsp;DEALER LOGIN</a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'installer')); ?>" class="btn btn-link backs_blue3 d-inline"><i class="fa fa-lock"></i> &nbsp;INSTALLER LOGIN</a>
            </div>
          </div>
          <div class="col-md-6 text-right">
            <div class="rights_tp_headers">
              <div class="d-inline bsx-choose_region">
                <select name="#" id="" class="form-control changes_branch_web"> 
                  <option value="http://www.carrier.co.id/">INDONESIA</option>
                  <option value="https://www.carrier.com/building-solutions/en/au/">AUSTRALIA</option>
                  <option value="https://www.carrier.com/building-solutions/en/cn/">CHINA</option>
                  <option value="http://www.carrier.com.hk/">HONGKONG</option>
                  <option value="https://www.carrier.com/building-solutions/en/in/">INDIA</option>
                  <option value="http://www.carrier.my/">MALAYSIA</option>
                  <option value="http://www.carrier.com.sg/">SINGAPORE</option>
                  <option value="https://www.carrier.com/building-solutions/en/tw/">TAIWAN</option>
                  <option value="http://www.carrier.co.th/">THAILAND</option>
                </select>

                <script type="text/javascript">
                  $( document ).ready(function() {
                    $('select.changes_branch_web').live('change', function(e){
                      e.preventDefault();
                      var urls_act = $(this).val();
                      window.open(urls_act, '_blank');
                    })
                  });
                </script>
              </div>
              <div class="box_search_hdr">
                <form action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>" method="GET" class="form-inline">
                  <div class="form-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..">
                  </div>
                  <button class="btn btn-link"><i class="fa fa-search"></i></button>
                </form>
                <div class="clear"></div>
              </div>
              <div class="bxn_right_language">
                <a href="#" class="active">EN</a>
                <!-- &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#">IN</a> -->
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="bottoms">
        <div class="row">
          <div class="col-md-3 col-lg-4">
            <div class="lgo_web_header"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>lgo-headers_carrier.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-md-9 col-lg-8">
            <div class="clear height-40"></div><div class="height-5"></div>
            <div class="top-menu">
              <ul class="list-inline">
                <li <?php echo ($active_menu_pg == 'home/index' || $active_menu_pg == '')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
                <li class="<?php echo ($controllers_ac == 'product' && $active_menu_pg != 'product/checkpart')? 'active':''; ?> dropdown first"><a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">products</a>
                   <ul class="dropdown-menu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
                    <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);
?>
                    <li class="dropdown">
                      <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                      <ul class="dropdown-menu">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                      </ul>
                    </li>
                    <?php endforeach ?>

                  </ul>
                </li>
                <li class="<?php echo ($active_menu_pg == 'product/checkpart')? 'active':''; ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/product/checkpart')); ?>">GENUINE PART VERIFICATION</a>
                </li>
                <li class="<?php echo ($active_menu_pg == 'home/services')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/services')); ?>">services</a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services')); ?>">Service and Maintenance</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'retrofit')); ?>">Service Retrofit/Optimize</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'parts-center')); ?>">Parts Center</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'warranty')); ?>">Warranty</a></li>
                  </ul>
                </li>
                <li class="<?php echo ($active_menu_pg == 'home/find_expert')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'dealer-location')); ?>">FIND AN EXPERT</a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'dealer-location')); ?>">Dealer Location</a></li>
                    <?php /*<li><a href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'asp-location')); ?>">ASP Location</a></li>*/ ?>
                  </ul>
                </li>
                <li <?php echo ($active_menu_pg == 'home/video')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/video')); ?>">VIDEOS</a></li>
                <li <?php echo ($active_menu_pg == 'home/event')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
                
                <li class="<?php echo ($active_menu_pg == 'home/about')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT</a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Carrier</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'core_value')); ?>">Core Values</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'fact_sheet')); ?>">Fact Sheet</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'willis_carrier')); ?>">Willis Carrier</a></li>
                  </ul>
                </li>
                <li <?php echo ($active_menu_pg == 'home/contact')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <!-- end bottoms header -->
      </div>

      <div class="clear"></div>
    </div>

  </div>
  <!-- end bottom -->
    
    <div class="visible-sm visible-xs">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
              <img src="<?php echo $this->assetBaseurl; ?>lgo-headers_carrier_res.png" alt="" class="img-responsive">
            </a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li <?php echo ($active_menu_pg == 'home/index' || $active_menu_pg == '')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
                <li class="<?php echo ($controllers_ac == 'product' && $active_menu_pg != 'product/checkpart')? 'active':''; ?> dropdown first"><a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">products</a>
                   <ul class="dropdown-menu">
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory = PrdCategory::model()->findAll($criteria);

?>
                    <?php foreach ($subCategory as $key => $value): ?>
<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = $value->id;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
// $criteria->limit = 3;
$criteria->order = 'sort ASC';
$subCategory2 = PrdCategory::model()->findAll($criteria);
?>
                    <li class="dropdown">
                      <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/product/landing', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                      <ul class="dropdown-menu">
                        <?php foreach ($subCategory2 as $k => $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                      </ul>
                    </li>
                    <?php endforeach ?>

                  </ul>
                </li>
                <li class="<?php echo ($active_menu_pg == 'product/checkpart')? 'active':''; ?>">
                  <a href="<?php echo CHtml::normalizeUrl(array('/product/checkpart')); ?>">GENUINE PART VERIFICATION</a>
                </li>
                <li class="<?php echo ($active_menu_pg == 'home/services')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/services')); ?>">services</a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services')); ?>">Service and Maintenance</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'retrofit')); ?>">Service Retrofit/Optimize</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'parts-center')); ?>">Parts Center</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/services', 'sub'=>'warranty')); ?>">Warranty</a></li>
                  </ul>
                </li>
                <li class="<?php echo ($active_menu_pg == 'home/find_expert')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'dealer-location')); ?>">FIND AN EXPERT</a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'dealer-location')); ?>">Dealer Location</a></li>
                    <?php /*<li><a href="<?php echo CHtml::normalizeUrl(array('/home/find_expert', 'loc'=>'asp-location')); ?>">ASP Location</a></li>*/ ?>
                  </ul>
                </li>
                <li <?php echo ($active_menu_pg == 'home/video')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/video')); ?>">VIDEOS</a></li>
                <li <?php echo ($active_menu_pg == 'home/event')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
                
                <li class="<?php echo ($active_menu_pg == 'home/about')? 'active':''; ?> dropdown first">
                  <a class="dropdown-toggle" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT</a>
                  <ul class="dropdown-menu onesub">
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Carrier</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'core_value')); ?>">Core Values</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'fact_sheet')); ?>">Fact Sheet</a></li>
                    <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'sub'=>'willis_carrier')); ?>">Willis Carrier</a></li>
                  </ul>
                </li>
                <li <?php echo ($active_menu_pg == 'home/contact')? 'class="active"':''; ?>><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
            </ul>
            <div class="clear height-10"></div>
            <div class="bloc_warranty_reg">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'warranty')); ?>" class="btn btn-link first">WARRANTY REGISTRATION</a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'dealer')); ?>" class="btn btn-link second"><i class="fa fa-lock"></i> &nbsp;DEALER LOGIN</a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/login', 'sub'=>'installer')); ?>" class="btn btn-link third"><i class="fa fa-lock"></i> &nbsp;INSTALLER LOGIN</a>
            </div>
            <div class="clear"></div>
             <div class="d-inline bsx-choose_region">
              <select name="#" id="" class="form-control">
                <option value="">INDONESIA</option>
                <option value="">AUSTRALIA</option>
                <option value="">THAILAND</option>
                <option value="">JAPAN</option>
              </select>
            </div>
            <div class="d-inline padding-left-20 bxn_right_language">
               <a href="#" class="active">EN</a>
                <!-- &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#">IN</a> -->
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </nav>
      <div class="clear"></div>
    </div>
    <!-- end bottom responsive -->
  <div class="clear"></div>
</header>
</div>
<?php /*
<script type="text/javascript">
  $(document).ready(function(){
      $('.nl_popup a').live('hover', function(){
          $('.popup_carts_header').fadeIn();
      });
      $('.popup_carts_header').live('mouseleave', function(){
        setTimeout(function(){ 
            $('.popup_carts_header').fadeOut();
        }, 500);
      });
  });
</script>

<script type="text/javascript">
    $(function(){
      $('#myAffix').affix({
        offset: {
          top: 300
        }
      })
    })
  </script>

<section id="myAffix" class="header-affixs affix-top">
  <div class="clear height-5"></div>
  <div class="prelatife container">
    <div class="row">
      <div class="col-md-3 col-sm-3">
        <div class="lgo-web-web_affix">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
            <img src="<?php echo $this->assetBaseurl ?>lgo_stumble_wt_txt.png" alt="" class="img-responsive d-inline" style="max-width: 75px;">
            STUMBLE UPON
          </a>
        </div>
      </div>
      <div class="col-md-9 col-sm-9">
        <div class="text-right"> 
          <div class="clear height-20"></div>
          <div class="menu-taffix">
            <ul class="list-inline">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">HOME</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">BUY COFFEE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/howto')); ?>">HOW TO ORDER</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/wholesale')); ?>">WHOLESALE</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">ABOUT US</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">EVENTS</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">CONTACT US</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/ ?>