<?php

/**
 * This is the model class for table "log_checker_uniqcode".
 *
 * The followings are the available columns in table 'log_checker_uniqcode':
 * @property string $id
 * @property string $ip_address
 * @property integer $promo_id
 * @property string $kode
 * @property string $date_check
 */
class LogCheckerUniqcode extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LogCheckerUniqcode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_checker_uniqcode';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('promo_id', 'numerical', 'integerOnly'=>true),
			array('ip_address, kode', 'length', 'max'=>225),
			array('date_check', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ip_address, promo_id, kode, date_check', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ip_address' => 'Ip Address',
			'promo_id' => 'Promo',
			'kode' => 'Kode',
			'date_check' => 'Date Check',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('promo_id',$this->promo_id);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('date_check',$this->date_check,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}