<?php
$this->breadcrumbs=array(
	'Testimonials',
);

$this->pageHeader=array(
	'icon'=>'fa fa-cogs',
	'title'=>'Setting',
	'subtitle'=>'Data Setting',
);

// $this->menu=array(
// 	array('label'=>' Add Testimonial', 'icon'=>'plus-sign','url'=>array('create')),
// );
?>

<?php // $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php 
	// if(Yii::app()->user->hasFlash('success')):

 //   $this->widget('bootstrap.widgets.TbAlert', array(
 //        'alerts'=>array('success'),
 //    )); 

    ?>

<?php // endif; ?>

<?php
$user_status = 'all';
// check user
$mod_user = User::model()->find('t.email = :emails', array(':emails'=>Yii::app()->User->id ));
if ($mod_user->group_id == 8 ) {
    $user_status = 'admin';
}
?>

<div class="row-fluid">
	<div class="span8">
		<h5 class="subtitle">Default Setting</h5>

                    <ul class="shortcuts setting">

                    	<?php if ($user_status != 'admin'): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/administrator/index')); ?>">
								<i class="icon-cms fa fa-user"></i>
                                <span class="shortcuts-label">Administrator Manager</span>
                        </a></li>
                    	<?php endif; ?>

		                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/defaultMetaPage/index')); ?>">
								<i class="icon-cms fa fa-key"></i>
                                <span class="shortcuts-label">Default Meta Page</span>
		                </a></li>
		                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/googleTools/index')); ?>">
								<i class="icon-cms fa fa-google"></i>
                                <span class="shortcuts-label">Google Tools</span>
		                </a></li>
		                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>">
								<i class="icon-cms fa fa-image"></i>
                                <span class="shortcuts-label">Slide</span>
		                </a></li>
                    </ul>
		
		<div class="clear" style="clear: both;"></div>
		<div class="divider10"></div>
	</div>
	<div class="span4">
		<?php $this->renderPartial('/setting/page_menu') ?>
	</div>
</div>
