<?php
$this->breadcrumbs=array(
	'Unique Code Generator'=>array('index'),
	'Edit Unique Code Generator',
);
$this->pageHeader=array(
	'icon'=>'fa fa-tag',
	'title'=>'Unique Code Generator',
	'subtitle'=>'Update Unique Code Generator',
);

$this->menu=array(
	array('label'=>'List Unique Code Generator', 'icon'=>'th-list', 'url'=>array('index')),
	array('label'=>'Add Unique Code Generator', 'icon'=>'plus-sign', 'url'=>array('create')),
	// array('label'=>'View Unique Code Generator', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelList'=>$modelList, 'numbs_batch'=> $model->batch_ke)); ?>