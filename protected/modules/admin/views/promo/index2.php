<?php
$this->breadcrumbs=array(
	'Top Verified Unique Code',
);
$this->pageHeader=array(
	'icon'=>'fa fa-tag',
	'title'=>'Top Verified Unique Code',
	'subtitle'=>'Top Verified Unique Code',
);

$this->menu=array(
	// array('label'=>'Add Top Verified Unique Code', 'icon'=>'plus-sign', 'url'=>array('create')),
);
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<?php $this->widget('bootstrap.widgets.TbAlert', array(
    'alerts'=>array('success'),
)); ?>
<?php endif; ?>

<!-- <a target="_blank" href="<?php // echo CHtml::normalizeUrl(array('promo/downloadExcel2')); ?>" class="btn"><i class="fa fa-download"></i> &nbsp;Download Excel</a> -->
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	// 'action'=>Yii::app()->createUrl('admin/promo/downloadExcel2'),
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row-fluid">
		<div class="span2">
			<?php echo $form->textFieldRow($model,'kode', array('class'=>'span12')); ?>
		</div>
		<div class="span2">
			<?php 
			$ar_filter = [
						'10'=>'Top 10',
						'50'=>'Top 50',
						'100'=>'Top 100',
						'all'=>'All',
						];
			?>
			<?php // echo $form->dropDownListRow($model,'choose_drop', $ar_filter, array('class'=>'span12','empty'=>'Pilih Filter')); ?>
			<label for="PromoList_choose_drop">Pilih Filter</label>
			<select class="span12" name="PromoList[choose_drop]" id="PromoList_choose_drop">
				<option value="">Pilih Filter</option>
				<?php foreach ($ar_filter as $key => $value): ?>
					<option <?php if (isset($_GET['PromoList']['choose_drop']) && $key == intval($_GET['PromoList']['choose_drop'])): ?>selected="selected"<?php endif ?> value="<?php echo $key ?>"><?php echo $value ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="span2">
			<label for="">&nbsp;</label>
			<?php // $model->t_excel = 1; ?>
			<?php echo $form->checkBoxRow($model,'t_excel'); ?>
		</div>
		<div class="span4">
			<label>&nbsp;</label>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'SUBMIT',
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				// 'buttonType'=>'button',
				'type'=>'info',
				'label'=>'RESET',
				'url'=>Yii::app()->createUrl($this->route),
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

<h1>Top Verified Unique Code</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	// 'htmlOptions'=>array('class'=>''),
	'columns'=>array(
		array(
			'header'=>'Batch Unique Code',
			'type'=>'raw',
			'value'=>' Promo::model()->findByPk($data->promo_id)->nama',
		),
		array(
			'header'=>'Tahun',
			'type'=>'raw',
			'value'=>'"20".Promo::model()->findByPk($data->promo_id)->tahun',
		),
		'kode',
		'terpakai',
		// 'min_pembelian',

	),
)); ?>

<?php /*
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>array(
	array('label'=>'Add Group', 'icon'=>'plus-sign', 'url'=>array('/admin/group/create')),
))); ?>

<h1>Groups</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'group-grid',
	'dataProvider'=>$model2->search(),
	// 'filter'=>$model2,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'group',
		array(
			'name'=>'aktif',
			'filter'=>array(
				'0'=>'No',
				'1'=>'Yes',
			),
			'value'=>'($data->aktif=="1")? "Yes" : "No" ',
		),
		// 'akses',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'deleteButtonUrl'=>"CHtml::normalizeUrl(array('/admin/group/delete', 'id'=>\$data->id))",
			'updateButtonUrl'=>"CHtml::normalizeUrl(array('/admin/group/update', 'id'=>\$data->id))",
		),
	),
)); ?>
*/ ?>

<script type="text/javascript">
	jQuery(function($){

		setTimeout(function(){ 
			location.reload();
			return false;
		  }, 60000);

		// $('.PromoList_choose_drop')
	})
</script>