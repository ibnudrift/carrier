<?php
$this->breadcrumbs=array(
	'Uniqcode Top View',
);
$this->pageHeader=array(
	'icon'=>'fa fa-tag',
	'title'=>'Uniqcode Top View',
	'subtitle'=>'Data Uniqcode Top View',
);

$this->menu=array(
	array('label'=>'Back', 'icon'=>'chevron-left', 'url'=>array('index')),
);
?>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>

<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>

<table class="items table table-bordered">
    <thead>
        <tr>
            <th id="user-grid_c0">No</th>
            <th id="user-grid_c1">Kode</th>
            <th id="user-grid_c2">Terpakai</th>
        </tr>
    </thead>
    <tbody>
    	<?php 
    	$no = 1;
    	?>
    	<?php foreach ($model as $key => $value): ?>
        <tr class="odd">
            <td><?php echo $no; ?></td>
            <td><?php echo $value->kode; ?></td>
            <td><?php echo $value->terpakai; ?></td>
        </tr>
        <?php $no++; ?>
    	<?php endforeach ?>

    </tbody>
</table>