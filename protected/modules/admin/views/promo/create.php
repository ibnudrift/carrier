<?php
$this->breadcrumbs=array(
	'Unique Code Generator'=>array('index'),
	'Add',
);
$this->pageHeader=array(
	'icon'=>'fa fa-tag',
	'title'=>'Unique Code Generator',
	'subtitle'=>'Tambah Unique Code Generator',
);

$this->menu=array(
	array('label'=>'List Unique Code Generator', 'icon'=>'th-list', 'url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'numbs_batch'=> $numbs_batch)); ?>