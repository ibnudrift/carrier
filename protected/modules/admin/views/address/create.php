<?php
$this->breadcrumbs=array(
	'Dealer & ASP Locator'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Dealer & ASP Locator',
	'subtitle'=>'Add Dealer & ASP Locator',
);

$this->menu=array(
	array('label'=>'List Dealer & ASP Locator', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>