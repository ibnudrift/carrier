<?php

class PromoController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter', 'params'=>array(
				'actionAllowOnLogin'=>array('edit'),
			)),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			): array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Promo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// check batch
		$numbs_batch = count( Promo::model()->findAll() ) + 1;
		$numb_var = sprintf("%02d", $numbs_batch);
		
		if(isset($_POST['Promo']))
		{
			$model->attributes=$_POST['Promo'];


			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->tgl_input = date("Y-m-d H:i:s");
					$model->kode = 'C'.$model->batch_ke . $model->tahun;

					$model->save();
					
					if ($model->jml_voucher > 1) {
						$data = Promo::Model()->getrand_code(intval($model->jml_voucher));
						$data1 = array_unique($data);

						// echo count($data1)."<br>";
						if (intval($model->jml_voucher) > 2500) {
							while(count($data1) < intval($model->jml_voucher)) {
								$data1 = array_unique( Promo::Model()->getrand_code(intval($model->jml_voucher)) );
							}
						}
						
						// echo count($data1);
						// echo "<pre>";
						// print_r($data1);
						// exit;

						foreach ($data1 as $key_co => $value_co) {
							$modelList = new PromoList;
							$modelList->promo_id = $model->id;
							$modelList->kode = strtoupper( $model->kode . $value_co );
							$modelList->save(false);
						}
					}

					Log::createLog("Promo Controller Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'numbs_batch'=>$numb_var,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$modelList = PromoList::model()->findAll('promo_id = :id', array(':id'=>$model->id));

		// check batch
		// $numbs_batch = count( Promo::model()->findAll() ) + 1;
		// $numb_var = sprintf("%02d", $numbs_batch);

		if(isset($_POST['Promo']))
		{
			$model->attributes=$_POST['Promo'];

			if ($model->validate()) {

				$model->kode = 'C'.$model->batch_ke . $model->tahun;

				$model->save();


				if ($model->jml_voucher != '') {
					PromoList::model()->deleteAll('promo_id = :id', array(':id'=>$model->id));
					if ($model->jml_voucher > 1) {
						$data = Promo::Model()->getrand_code(intval($model->jml_voucher));
						$data1 = array_unique($data);
						if (intval($model->jml_voucher) > 2500) {
							while(count($data1) < intval($model->jml_voucher)) {
								$data1 = array_unique( Promo::Model()->getrand_code(intval($model->jml_voucher)) );
							}
						}

						foreach ($data1 as $key_co => $value_co) {
							$modelList = new PromoList;
							$modelList->promo_id = $model->id;
							$modelList->kode = strtoupper( $model->kode . $value_co );
							$modelList->save(false);
						}
					}
				}

				Yii::app()->user->setFlash('success',Tt::t('admin', 'Data Edited'));
				Log::createLog("Promo Update $model->id");
				$this->redirect(array('index'));
			}
		}
		$this->render('update',array(
			'model'=>$model,
			'modelList'=>$modelList,
			'numbs_batch'=>$numb_var,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$data = $this->loadModel($id);
			PromoList::model()->deleteAll('promo_id = :id', array(':id'=>$data->id));
			
			Log::createLog("Promo Delete $data->id");
			$data->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
				$this->redirect(array('index'));
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Promo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Promo']))
			$model->attributes=$_GET['Promo'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionIndex2()
	{
		$model=new PromoList('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PromoList']))
			$model->attributes=$_GET['PromoList'];

		if ( isset($_GET['PromoList']['t_excel']) && $_GET['PromoList']['t_excel'] == "1") {
			$n_limit = intval($_GET['PromoList']['choose_drop']);
			$this->redirect(array('Downloadexcel2', 'limit_data'=> $n_limit ));
		}

		$this->render('index2',array(
			'model'=>$model,
		));
	}

	public function actionViewtopten()
	{
		$ids = intval($_GET['id']);

		$criteria = new CDbCriteria;
		$criteria->addCondition('t.promo_id = :ids');
		$criteria->params[':ids'] = $ids;
		$criteria->order = 't.terpakai DESC';
		$criteria->limit = 10;

		$modelList = PromoList::model()->findAll($criteria);

		$this->render('top_view',array(
			'model'=>$modelList,
		));
	}

	public function actionDownloadexcel()
	{
		$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
		$id = intval($_GET['id']);
		$data_parent = Promo::model()->findByPk($id);
		$data = PromoList::model()->findAll('t.promo_id = :par_id', array(':par_id'=>$data_parent->id));

		// $filename = "Carrier_exports_".$data_parent->nama.".xls";		 
			// header("Content-type: application/vnd-ms-excel");
			// header("Content-Disposition: attachment; filename=\"$filename\"");
			
			// $str = '<table border="1"><tr><td>Uniqcode System</td><td width="510">QR Code Link</td></tr>';

			// foreach ($data as $key => $value) {
			// 	$str .= '<tr>';
			// 	$str .= '<td>'. $value->kode .'</td>';
			// 	$str .= '<td>'.$baseUrl.'/verify_genuine?code='.$value->kode .'&stcheck=1</td>';
			// 	$str .= '</tr>';
			// }
			// $str .= '</table>';
			// echo $str;

		$filename = "Carrier_exports_".$data_parent->nama;
		// $filename = "Carrier_exports"; 
		$transaction_data = array();
		$transaction_data[] = array(
								0 => 'Uniqcode System',
								1 => 'QR Code Link',
								);

		foreach ($data as $key => $value) {
			$transaction_data[] = array(
									$value->kode,
									$baseUrl.'/verify_genuine?code='.$value->kode .'&stcheck=1',
								  );
		}
		$json_transaction_data2 = json_encode($transaction_data, JSON_PRETTY_PRINT);
		// echo "<pre>"; print_r($transaction_data); die(0);

		$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
		$url = Yii::app()->request->hostInfo;

		$curl = curl_init();
		curl_setopt_array($curl, [
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $baseUrl.'/php-xlsx/sent.php',
		    CURLOPT_POST => 1,
		    CURLOPT_POSTFIELDS => ['data'=>$json_transaction_data2, 'filename'=> $filename],
		]);
		$response = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($response); 

		// echo "<pre>"; var_dump($response); exit;

		header('Content-disposition: attachment; filename="'.$filename.'_'.rand(1, 50).'.xlsx"');
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		readfile($response->result_file);
		exit;

	}

	public function actionDownloadexcel2()
	{
		set_time_limit(0);
		// $limits = $_GET['PromoList']['choose_drop'];
		$limits = $_GET['limit_data'];
		$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
		// $id = intval($_GET['id']);
		// $data_parent = Promo::model()->findByPk($id);
		$criteria=new CDbCriteria;
		$criteria->order = 't.terpakai DESC';


		if (isset($limits) && $limits != 0) {
			$criteria->limit = intval($limits);
		}

		$data = PromoList::model()->findAll($criteria);

		// $filename = "Carrier_exports_".$data_parent->nama.".xls";		 
			// 	header("Content-type: application/vnd-ms-excel");
			// 	header("Content-Disposition: attachment; filename=\"$filename\"");
				
			// 	$str = '<table border="1"><tr><td>Batch Name</td><td>Uniqcode System</td><td>Terpakai</td><td>Tahun</td><td width="510">QR Code Link</td></tr>';

			// 	foreach ($data as $key => $value) {
			// 		$mod_par = Promo::model()->findByPk($value->promo_id);
			// 		$str .= '<tr>';
			// 		$str .= '<td>'. $mod_par->nama .'</td>';
			// 		$str .= '<td>'. $value->kode .'</td>';
			// 		$str .= '<td>'. $value->terpakai .'</td>';
			// 		$str .= '<td>'. '20'. $mod_par->tahun .'</td>';
			// 		$str .= '<td>'.$baseUrl.'/verify_genuine?code='.$value->kode .'&stcheck=1</td>';
			// 		$str .= '</tr>';
			// 	}
			// 	$str .= '</table>';
			// 	echo $str;

		if (isset($limits) && $limits != 0) { 
			$filename = "Carrier_exports_report_Top".$limits;
		}else{
			$filename = "Carrier_exports_report_All";
		}
		$transaction_data = array();
		$transaction_data[] = array(
								0 => 'Batch Name',
								1 => 'Uniqcode System',
								2 => 'Terpakai',
								3 => 'Tahun',
								4 => 'QR Code Link',
								);

		foreach ($data as $key => $value) {
			$mod_par = Promo::model()->findByPk($value->promo_id);
			$transaction_data[] = array(
									$mod_par->nama,
									$value->kode,
									$value->terpakai,
									'20'. $mod_par->tahun,
									$baseUrl.'/verify_genuine?code='.$value->kode .'&stcheck=1',
								  );
		}
		$json_transaction_data2 = json_encode($transaction_data, JSON_PRETTY_PRINT);
		// echo "<pre>"; print_r($transaction_data); die(0);

		$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
		$url = Yii::app()->request->hostInfo;

		$curl = curl_init();
		curl_setopt_array($curl, [
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $baseUrl.'/php-xlsx/sent.php',
		    CURLOPT_POST => 1,
		    CURLOPT_POSTFIELDS => ['data'=>$json_transaction_data2, 'filename'=> $filename],
		]);
		$response = curl_exec($curl);
		curl_close($curl);

		$response = json_decode($response); 
		// echo "<pre>";
		// var_dump($response);

		header('Content-disposition: attachment; filename="'.$filename.'_'.rand(1, 50).'.xlsx"');
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		readfile($response->result_file);
		exit;
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Promo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Promo']))
			$model->attributes=$_GET['Promo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Promo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
